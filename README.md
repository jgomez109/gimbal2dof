# Gimbal2DOF
- Los archivos .m se realizaron en Matlab v2020b.
- Los archivos .slx se realizaron en Simulink v2020b.
- Los directorios PID, LQG y MixSyn se abren mediante Platformio. 
- Para correr los programas mediante platformio se requiere tener la librería BasicLinearAlgebra V2.3 realizada por Tom Stewart.
- Por defecto cada programa se compila y se envía a la ESP32 por el puerto serial COM3, puede modificar esto en el archivo platformio.ini (respectivo en cada directorio), como se muestra a continuación:
```
upload_port = COM[3]
lib_deps =
    BasicLinearAlgebra 
```


