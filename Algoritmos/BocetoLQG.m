%% Modelo No lineal
clear all
clc

syms Va1 Vb1 Vc1 Va2 Vb2 Vc2        % Voltajes de linea
syms ia1 ib1 ic1 ia2 ib2 ic2        % Corrientes de linea
syms te1 te2        % Ángulo eléctrico(te)
syms Te1 Tl1 Te2 Tl2        % Torque eléctrico y carga
syms R1  R2     % Iesistencia de linea
syms Kt1 Kt2        % Constante de torque [N*m/A]
syms b1 b2      % Constante de amortiguamiento (Fricción) [N*m/(Rad/s)]
syms P1  P2     % Número de polos del motor

syms u1 u2 %Indices (Ángulos eléctricos de control)

syms x1 y1 z1 % Centros de masa eslabon 1
syms x2 y2 z2 % Centros de masa eslabon 2
syms q1 q1p q1pp q2 q2p q2pp  % Posición, velocidad, aceleración eslabón 1 y 2
syms m1 m2 I1 I2 g % Masas, inercias eslabón 1 y 2; gravedad.
syms l1 l2 l3  % Distancias entre origenes de los Sistema de referencia
syms Kv1 Kv2 % Constantes para el escalado de la amplitud del voltaje en los motores
syms Vmax % Voltaje aplicado a los motores

%Relación angulo eléctrico y mecánico motor 1
qe1 = P1*q1/2 
%Relación angulo eléctrico y mecánico motor 2
qe2 = P2*q2/2

%Conversión de indice ángulo
ae1 = u1*2*pi/256;
ae2 = u2*2*pi/256;

%Voltajes Aplicados en función de la acción de control u1, Motor 1
Va1 = 0.5*Kv1*Vmax*sin(ae1)          + 0.5*Vmax
Vb1 = 0.5*Kv1*Vmax*sin(ae1 + 2*pi/3) + 0.5*Vmax
Vc1 = 0.5*Kv1*Vmax*sin(ae1 + 4*pi/3) + 0.5*Vmax

%Voltajes Aplicados en función de la acción de control u1, Motor 2
Va2 = 0.5*Kv2*Vmax*sin(ae2)          + 0.5*Vmax
Vb2 = 0.5*Kv2*Vmax*sin(ae2 + 2*pi/3) + 0.5*Vmax
Vc2 = 0.5*Kv2*Vmax*sin(ae2 + 4*pi/3) + 0.5*Vmax

%Corrientes fasoriales Motor 1
ia1 = Va1/R1
ib1 = Vb1/R1
ic1 = Vc1/R1
%Corrientes fasoriales Motor 2
ia2 = Va2/R2
ib2 = Vb2/R2
ic2 = Vc2/R2

%Torque a mover en el eslabón 1
T1 = I1*q1pp + I2*q1pp + I2*q2pp + b1*q1p + l1^2*m1*q1pp + l1^2*m2*q1pp + l3^2*m2*q1pp + m1*q1pp*x1^2 + m1*q1pp*y1^2 + m2*q1pp*y2^2 + m2*q1pp*z2^2 + g*m2*(l1*sin(q1) - z2*sin(q1) + x2*cos(q1)*cos(q2) + l3*cos(q1)*sin(q2) - y2*cos(q1)*sin(q2)) - g*m1*(y1*cos(q1) - l1*sin(q1) + x1*sin(q1)) - 2*l1*m1*q1pp*x1 - 2*l3*m2*q1pp*y2 - 2*l1*m2*q1pp*z2 - l3^2*m2*q1pp*cos(q2)^2 + m2*q1pp*x2^2*cos(q2)^2 - m2*q1pp*y2^2*cos(q2)^2 + m2*q2pp*y2*z2*cos(q2) - l1*m2*q2pp*x2*sin(q2) + m2*q2pp*x2*z2*sin(q2) + 2*l3*m2*q1pp*y2*cos(q2)^2 + l3*m2*q1pp*x2*sin(2*q2) - m2*q1pp*x2*y2*sin(2*q2) + l1*l3*m2*q2pp*cos(q2) - l1*m2*q2pp*y2*cos(q2) - l3*m2*q2pp*z2*cos(q2);
%Toque a mover en el eslabón 2
T2 = I2*q1pp + I2*q2pp + b2*q2p + l3^2*m2*q2pp + m2*q2pp*x2^2 + m2*q2pp*y2^2 - (m2*q1p*(l3^2*q1p*sin(2*q2) - q1p*x2^2*sin(2*q2) + q1p*y2^2*sin(2*q2) - 2*l1*q2p*x2*cos(q2) - 2*l1*l3*q2p*sin(q2) + 2*q2p*x2*z2*cos(q2) + 2*l1*q2p*y2*sin(q2) + 2*l3*q2p*z2*sin(q2) - 2*q2p*y2*z2*sin(q2) + 2*l3*q1p*x2*cos(2*q2) - 2*q1p*x2*y2*cos(2*q2) - 2*l3*q1p*y2*sin(2*q2)))/2 - 2*l3*m2*q2pp*y2 - g*m2*sin(q1)*(y2*cos(q2) - l3*cos(q2) + x2*sin(q2)) + m2*q1pp*y2*z2*cos(q2) - l1*m2*q1pp*x2*sin(q2) + m2*q1pp*x2*z2*sin(q2) + l1*l3*m2*q1pp*cos(q2) - l1*m2*q1pp*y2*cos(q2) - l3*m2*q1pp*z2*cos(q2);

%Dinámica mecánica (Balance de Torques en el eje del motor 1)
ecu5 = T1 ==  Kt1*( sin(qe1)*ia1 + sin(qe1 + 2*pi/3)*ib1 + sin(qe1 + 4*pi/3)*ic1 )
%Dinámica mecánica (Balance de Torques en el eje del motor 2)
ecu6 = T2 ==  Kt2*( sin(qe2)*ia2 + sin(qe2 + 2*pi/3)*ib2 + sin(qe2 + 4*pi/3)*ic2 )

%Despejar aceleraciones angulares
[q1pp,q2pp] = solve(ecu5,ecu6,q1pp,q2pp);
q1pp = simplify(q1pp)       % Razón de cambio q1p
q2pp = simplify(q2pp)       % Razón de cambio q2p

x = [q1 ; q1p ; q2 ; q2p]       % ESTADOS
X = [q1p; q1pp; q2p; q2pp]      % DINÁMICAS
u = [u1; u2]        % ENTRADAS DE CONTROL 
y = [q1; q2]        % SALIDAS

A = jacobian(X,x)       % (dX/dx)
B = jacobian(X,u)       % (dX/du)
% C = jacobian(y,x)     % (dy/dx) (Radianes)
C = [1*180/pi 0 0 0     % (dy/dx) (Grados)
     0        0 180/pi 0]
D = jacobian(y,u)       % (dy/du)

%% Modelo Lineal
%Razones de cambio igual a cero
q1p = 0
q2p = 0
%Punto de equilibrio
u1 =   512  
u2 =   -384 
q1 =   (-90)*pi/180
q2 =   ( 90)*pi/180

%Valores de los parámetros
R1 = 15.445
R2 =  15.445
P1 = 14
P2  =14
Kv1 = 126.16/255
Kv2 = 112.88/255
x1 =-0.011
y1 =0
z1 =0.0047
x2 =0.00065
y2 =0.018
z2 =0.031
l1 =0.01625
l2 =0.03475
l3 =0.02275
g  =9.81
Vmax = 12
Kt1 = 0.11
b1 =0.0007
Kt2 = 0.11
b2 =0.0007
m1 =0.062  
m2 =0.0777
I1 =0.00005182988
I2 =0.00001912189

%Modelo lineal en espacio de estados
AL = double(subs(A))
BL = double(subs(B))
CL = double(subs(C))
DL = double(subs(D))
sys = ss(AL,BL,CL,DL)
%Modelo lineal en espacio de estados discreto
sysd = c2d(sys,0.001,'zoh')
%Comparación de la respuesta continúa y discreta
step(sys,sysd)
%Matrices A y B discretizadas
GL = sysd.a
HL = sysd.b

%% Definción de entradas, salidas y estados:
disp(' ')
disp('Número de Entradas:')
p = 2
disp('Número de Salidas:')
r = 2  %Número de salidas
disp('Número de estados:')
n = 4
%% Matriz de controlabilidad en modo Seguidor
Ahat = [AL,zeros(n,r);
       -CL,zeros(r,r)] 
Bhat = [BL;
           -DL]
disp('Matriz de controlabilidad tipo seguidor:')
Mc = ctrb(Ahat,Bhat) %Verifico la controlabilidad
disp('Rango Matriz de controlabilidad tipo seguidor:')
rank(Mc) % Controlable si rank(Mc) = estados + salidas
%% Diseño Controlador LQR
Q = diag([258800000000 10000 10000000000 7000 5850000000000 388880000000]) % Xi + Yi
R = diag([5000 1000]) %Ui
[Kt,S,E] =lqr(Ahat,Bhat,Q,R) % solución asociada a la ecuación de Riccati kt: Ganancias, S:Solución asociada a la ecuación, E: Polos del controlador o Eigenvalores (Ahat-Bhat*Kt)
Kest = Kt(:,1:n) % Ganancias de los estados
Ki = -Kt(:,n+1:end) % Ganancias de los integradores de control para cada una de las salidas

%% Simulación en lazo cerrado controlador LQR.
%Matrices en lazo cerrado
Anew = [AL-BL*Kest,BL*Ki; 
        -CL+DL*Kest,-DL*Ki]
Bnew = [zeros(n,r);
        eye(r)]
Cnew = [CL-DL*Kest, DL*Ki] 
Dnew = zeros(r,r)
sysnew = ss(Anew,Bnew,Cnew,Dnew)
Cu =[-Kest,Ki]
Du = zeros(p,r)
sysu = ss(Anew,Bnew,Cu,Du)

% Gráficas:
figure(1)
step(sysnew)
title('Lazo cerrado Y_i (Seguidor de trayectoria)')
grid
figure(2)
step(sysu)
title('Lazo cerrado U_i (Seguidor de trayectoria)')
grid
disp('Valores propios en lazo cerrado:')
eig(Anew)      
%% Matriz de observabilidad tipo Seguidor
% Vob = [CL; CL*AL; CL*AL^2]
Vob = obsv(AL,CL)
rank(Vob) % Observable si rank(Vob) = estados 
%% Observador de Kalman 
Qn = diag([0.000001^2,0.000001^2]) % Ui
Rn = diag([0.1^2 0.1^2]) % Yi
sys_kal = ss(AL,[BL BL],CL,[DL,DL])
[Kobv,L,P] = kalman(sys_kal,Qn,Rn)
eig(Kobv.a)  % Polos del observador (Deben estar por detras del controlador)
E  % Polos del controlador (Polos del controlador LQR del sistema (diseñado arriba)   
%% Controlador LQG:
%Unificación "1DOF" del controlador LQR y el observador de Kalman:
Ac_1 = [AL-BL*Kest-L*CL+L*DL*Kest, BL*Ki - L*DL*Ki;
        zeros(r,n), zeros(r,r)];
Bc_1 = [-L;
        eye(r)];  
Cc_1 = [-Kest, Ki];    
Dc_1 = zeros(p,r);

%Controlador en espacio de estados:
sys_c1 = ss(Ac_1,Bc_1,Cc_1,Dc_1) 
sys_c1d = c2d(sys_c1,0.001) % Controlador en espacio de estados discretizado a 1 ms

%% Simulación en lazo cerrado controlador LQG.
ALC1 = [AL,BL*Cc_1;
       -Bc_1*CL,(Ac_1-Bc_1*DL*Cc_1)]
BLC1 = [zeros(n,r);
        Bc_1]
CLC1 = [CL,DL*Cc_1]
DLC1 = zeros(r,r)
sysLC1 = ss(ALC1,BLC1,CLC1,DLC1)
sysLC1d = c2d(sysLC1,0.001)
% Lazo = feedback(sys_c1*sys,eye(2)) %eye(variables a controlar) % No permite cuando las salidas son menores al las acciones de control
% Accion = feedback(sys_c1,sys);
CuLC1 = [zeros(p,n),Cc_1] 
DuLC1 = zeros(p,r)
sysuLC1 = ss(ALC1,BLC1,CuLC1,DuLC1)
sysuLC1d = c2d(sysuLC1,0.001)
figure(3)
sysLC1.InputName = {'e1', 'e2'}
sysLC1.OutputName = {'Eslabón 1', 'Eslabón 2'}
step(sysLC1,sysLC1d)
legend('Continuo', 'Discreto')
title('LQG Yi')
figure(4)
sysuLC1.InputName = {'e1', 'e2'}
sysuLC1.OutputName = {'u1', 'u2'}
step(sysuLC1,sysuLC1d)
legend('Continuo', 'Discreto')
title('LQG Ui')  
%% Impresión de las matrices del controlador en espacio de estados discreto
fprintf('Ac:\n')
fprintf('%.10f,\t',sys_c1d.a(1,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.a(2,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.a(3,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.a(4,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.a(5,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.a(6,:)); fprintf('\n')
fprintf('Bc:\n')
fprintf('%.10f,\t',sys_c1d.b(1,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.b(2,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.b(3,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.b(4,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.b(5,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.b(6,:)); fprintf('\n')
fprintf('\n')
fprintf('Cc:\n')
fprintf('%.10f,\t',sys_c1d.c(1,:)); fprintf('\n')
fprintf('%.10f,\t',sys_c1d.c(2,:)); fprintf('\n') 
% Dc = zeros(2,2)