clear all
clc

syms Vab Vbc Va Vb Vc   % Voltajes de linea
syms ia ib ic           % Corrientes de linea
syms tm te              % Ángulo mecánico (tm) y eléctrico(te)
syms ea eb ec           % Voltajes BEMF
syms Te Tl              % Torque eléctrico y carga
syms Ia Ib Ic Wm Am     % Razones de cambio Ia = dia/dt, Ib = dib/dt, 
                        % Ic = dic/dt, Wm = dtm/dt, Am, dtWm/dt
syms L R                % Inductancia y resistencia de linea
syms Ke                 % Constante BEMF [V/(rad/s)]
syms Kt                 % Constante de torque [N*m/A]
syms Kf                 % Constante de amortiguamiento (Fricción) [N*m/(Rad/s)]
syms J                  % Inercia del rotor [N*m/(rad/s^2)=kg*m^2]
syms P                  % Número de polos del motor

te = P*tm/2

% Voltajes BEMF de acuerdo con el ángulo ELÉCTRICO
ea = Ke*Wm*sin(te)/2
eb = Ke*Wm*sin(te - 2*pi/3)/2
ec = Ke*Wm*sin(te - 4*pi/3)/2

% Voltajes BEMF de línea a línea
eab = ea-eb
ebc = eb-ec

%Fase C en función de A y B, balance de neutro
Ic = -Ia - Ib
ic = -ia - ib

% Voltajes de línea a línea
Vab = Va-Vb
Vbc = Vb-Vc

% Dinámicas eléctricas
ecu1 = Vab == R*(ia-ib) + L*(Ia-Ib) + ea - eb
ecu2 = Vbc == R*(ib-ic) + L*(Ib-Ic) + eb - ec

% Dinámica mecánica (Balance de Torques en el eje del motor)
ecu3 = Kf*Wm + J*Am + Tl == Kt*(sin(te)*ia + sin(te - 2*pi/3)*ib + sin(te - 4*pi/3)*ic)/2

% Despejamos simplificamos Dinámicas de Corrientes de Fase y Aceleración
% del motor
[Ia,Ib] = solve(ecu1,ecu2,Ia,Ib);
Ia = simplify(Ia)
Ib = simplify(Ib)
Am = solve(ecu3,Am);
Am = simplify(Am)

X = [Ia;Ib;Am;Wm]       % DINÁMICAS
x = [ia;ib;Wm;tm]       % ESTADOS
u = [Va;Vb;Vc;Tl]       % ENTRADAS DE CONTROL + PERTURBACIÓN (Tl -->torque de carga)


A = jacobian(X,x)       % LINEALIZACIÓN ESTADOS  (dX/dx)
B = jacobian(X,u)       % LINEALIZACIÓN ENTRADAS (dX/du)
C =  [ 1  0 0 0;         
      0  1 0 0;
     -1 -1 0 0;
      0  0 1 0;
      0  0 0 1]
% Cm = [ 1  0 0 0;         % Y = [ia, ib, ic, Wm, tm, Te]
%       0  1 0 0;
%      -1 -1 0 0;
%       0  0 1 0;
%       0  0 0 1;      
%       -Kt*(sqrt(3)*cos(P*tm/2)+3*sin(P*tm/2))/4,  -Kt*(sqrt(3)*cos(P*tm/2))/2, 0, 0]
Dm = zeros(6,4)
% Con base en ia, ib, ic y tm se calcula el torque utilizando cualquiera de las siguientes ecuaciones:
% Te = Kt*(sin(te)*ia + sin(te - 2*pi/3)*ib + sin(te - 4*pi/3)*ic)/2
% Te = -(Kt*((3^(1/2)*ia*cos((P*tm)/2))/2 - (3*ia*sin((P*tm)/2))/2 + 3^(1/2)*ib*cos((P*tm)/2)))/2