clear all
clc

syms t q1(t) q2(t) l1 l2 l3

%% Matices de transformación:
% Primer eslabón:
Tz1 = [rotz(180),zeros(3,1);
        zeros(1,3),1]
    
Ty1 = [roty(-90),zeros(3,1);
        zeros(1,3),1]    
    
Rq1 = [cos(q1),-sin(q1),0;
       sin(q1),cos(q1),0
       0,0,1]
   
Tzq1 = [Rq1,zeros(3,1);
       zeros(1,3),1]
   
Td1 = [eye(3,3),[-l2;l1*sin(q1);-l1*cos(q1)]; % La translación vista desde 0, debe incluir las rotaciones que se realizarón para llegar a 1.
       zeros(1,3),1]

T01 = Td1*Tz1*Ty1*Tzq1; % T01


% Segundo eslabón:

Tz2 = [rotz(-90),zeros(3,1);
        zeros(1,3),1]
    
Tx2 = [rotx(-90),zeros(3,1);
        zeros(1,3),1]    
    
Rq2 = [cos(q2),-sin(q2),0;
       sin(q2),cos(q2),0
       0,0,1]
   
Tzq2 = [Rq2,zeros(3,1);
       zeros(1,3),1]
   
Td2 = [eye(3,3),[0;-l3*sin(q2);l3*cos(q2)]; % La translación vista desde 1, debe incluir las rotaciones que se realizarón para llegar a 2.
       zeros(1,3),1]

T12 = Td2*Tz2*Tx2*Tzq2; % T12

T02 = T01*T12; % T02

T01 = simplify(T01);pretty(T01)
T12 = simplify(T12);pretty(T12)
T02 = simplify(T02);pretty(T02)
%% Centros de masa de los eslabones:
% Primer eslabón:
% Distancias medidas desde el sistema referencia 1:
syms x1 y1 z1
rc1 = [x1;y1;z1;1]

% Segundo eslabón: 
% Distancias medidas desde el sistema referencia 2:
syms x2 y2 z2
rc2 = [x2;y2;z2;1]
%% Velocidad de los centros de masa:
% Medidos desde el sistema de referencia 0:
% Primer eslabón:
V01 = diff(T01,t)*inv(T01);
vc1 = V01*(T01*rc1);vc1 = simplify(vc1)
vc1 = vc1(t);
% Segundo eslabón:
V02 = diff(T02,t)*inv(T02);
vc2 = V02*(T02*rc2);vc2 = simplify(vc2)
vc2 = vc2(t);
%% Posición de los centros de masa:
% Medidos desde el sistema de referencia 0:
% Primer eslabón:
p01 = T01*rc1;
p01 = p01(t)
p02 = T02*rc2;
p02 = p02(t)
%% Cambio de symfun a sym para encontrar la magnitud de la velocidad y los desplazamientos de la posición
syms a b c d 
v1 = subs(vc1,{q1(t), diff(q1(t), t)},{a,b});
v2 = subs(vc2,{q1(t), diff(q1(t), t), q2(t), diff(q2(t), t)},{a,b,c,d});
p01 = subs(p01,{q1(t)},{a});
p02 = subs(p02,{q1(t),q2(t)},{a,b});

syms q1 q1p q2 q2p 
v1 = subs(v1,{a, b},{q1,q1p});
v2 = subs(v2,{a, b, c, d},{q1,q1p,q2,q2p});
p01 = subs(p01,{a},{q1})
p02 = subs(p02,{a,b},{q1,q2})

clearvars a b c d
%% Velocidades al cuadrado
v1_2 = v1(1)*v1(1) + v1(2)*v1(2) +v1(3)*v1(3);
v1_2 = simplify(v1_2)
v2_2 = v2(1)*v2(1) + v2(2)*v2(2) +v2(3)*v2(3);
v2_2 = simplify(v2_2)
%% Desplazamiento en el eje vertical
z01 = p01(3)
z02 = p02(3)
%% Lagrange:
syms m1 m2 I1 I2 g
% T1 = (1/2)*((m1*v1_2) + I1*q1p^2)
% T2 = (1/2)*((m2*v2_2) + I2*(q2p+q1p)^2)
T1 = (1/2)*((m1*v1_2) + I1*(q1p^2)^2)
T2 = (1/2)*((m2*v2_2) + I2*(q2p^2+q1p^2)^2)

U1 = m1*g*z01
U2 = m2*g*z02
T = T1+T2
U = U1+U2
L = T-U;
L = simplify(L)

% Derivadas parciales
DL1P = jacobian(L,q1p);
DL1P = simplify(DL1P)
DL2P = jacobian(L,q2p);
DL2P = simplify(DL2P)

DL1 = jacobian(L,q1);
DL1 = simplify(DL1)
DL2 = jacobian(L,q2);
DL2 = simplify(DL2)

% Derivadas en el tiempo
% Cambio de sym a symfun para desarrollar las derivadas en el tiempo
syms a b c d
DL1Pt = subs(DL1P,{q1, q1p, q2, q2p},{a,b,c,d});
DL2Pt = subs(DL2P,{q1, q1p, q2, q2p},{a,b,c,d});
syms q1(t) q2(t) q1p(t) q2p(t) t 
DL1Pt = diff(subs(DL1Pt,{a, b, c, d},{q1,q1p,q2,q2p}),t);
DL1Pt = simplify(DL1Pt)
DL2Pt = diff(subs(DL2Pt,{a, b, c, d},{q1,q1p,q2,q2p}),t);
DL2Pt = simplify(DL2Pt)
clearvars a b c d

% Cambio de symfun a sym para establecer las ecuaciones
syms a b c d e f
DL1Pt = subs(DL1Pt,{q1, q1p, diff(q1p(t), t), q2, q2p, diff(q2p(t), t)},{a,b,c,d,e,f});
DL2Pt = subs(DL2Pt,{q1, q1p, diff(q1p(t), t), q2, q2p, diff(q2p(t), t)},{a,b,c,d,e,f});
syms q1 q1p q1pp q2 q2p q2pp
DL1Pt = subs(DL1Pt,{a,b,c,d,e,f},{q1,q1p,q1pp,q2,q2p,q2pp});
dDL1P = simplify(DL1Pt)
DL2Pt = subs(DL2Pt,{a,b,c,d,e,f},{q1,q1p,q1pp,q2,q2p,q2pp});
dDL2P = simplify(DL2Pt)
clearvars a b c d e f
%% Ecuaciones:
syms  b1 b2  torque1 torque2  
ecu1 = dDL1P - DL1 + b1*q1p == torque1
ecu2 = dDL2P - DL2 + b2*q2p == torque2

[Q1pp, Q2pp] = solve(ecu1,ecu2,q1pp,q2pp)
% Q1pp: (2*I2*u(5) + 2*l3^2*m2*u(5) + 2*m2*u(5)*x2^2 + 2*m2*u(5)*y2^2 - 2*I2*b1*u(2) - 4*l3*m2*u(5)*y2 - 2*b1*l3^2*m2*u(2) - 2*b1*m2*u(2)*x2^2 - 2*b1*m2*u(2)*y2^2 - 2*m2*u(6)*y2*z2*cos(u(3)) + 2*l1*m2*u(6)*x2*sin(u(3)) - 2*g*m2^2*x2^3*cos(u(1))*cos(u(3)) - 2*g*l3^3*m2^2*cos(u(1))*sin(u(3)) - 2*m2*u(6)*x2*z2*sin(u(3)) + 2*g*m2^2*y2^3*cos(u(1))*sin(u(3)) + 4*b1*l3*m2*u(2)*y2 - 2*g*l1*l3^2*m2^2*sin(u(1)) - 2*g*l1*m2^2*x2^2*sin(u(1)) - 2*g*l1*m2^2*y2^2*sin(u(1)) + 2*g*l3^2*m2^2*z2*sin(u(1)) + 2*g*m2^2*x2^2*z2*sin(u(1)) + 2*g*m2^2*y2^2*z2*sin(u(1)) + 2*I2*g*m1*y1*cos(u(1)) - 2*I2*g*l1*m1*sin(u(1)) - 2*I2*g*l1*m2*sin(u(1)) + 2*I2*g*m1*x1*sin(u(1)) + 2*I2*g*m2*z2*sin(u(1)) - 2*l1*l3*m2*u(6)*cos(u(3)) + 2*l1*m2*u(6)*y2*cos(u(3)) + 2*l3*m2*u(6)*z2*cos(u(3)) - 2*g*m2^2*x2^2*z2*sin(u(1))*sin(u(3))^2 + 2*b2*l1*l3*m2*u(4)*cos(u(3)) - l1*l3^3*m2^2*u(2)^2*sin(2*u(3))*cos(u(3)) - 2*b2*l1*m2*u(4)*y2*cos(u(3)) + l1*m2^2*u(2)^2*y2^3*sin(2*u(3))*cos(u(3)) - 2*b2*l3*m2*u(4)*z2*cos(u(3)) + l3^3*m2^2*u(2)^2*z2*sin(2*u(3))*cos(u(3)) + 2*b2*m2*u(4)*y2*z2*cos(u(3)) - m2^2*u(2)^2*y2^3*z2*sin(2*u(3))*cos(u(3)) - 2*b2*l1*m2*u(4)*x2*sin(u(3)) - l1*m2^2*u(2)^2*x2^3*sin(2*u(3))*sin(u(3)) + 2*b2*m2*u(4)*x2*z2*sin(u(3)) + m2^2*u(2)^2*x2^3*z2*sin(2*u(3))*sin(u(3)) - 2*g*l3^2*m2^2*x2*cos(u(1))*cos(u(3)) - 2*g*m2^2*x2*y2^2*cos(u(1))*cos(u(3)) - 2*g*l3*m2^2*x2^2*cos(u(1))*sin(u(3)) - 6*g*l3*m2^2*y2^2*cos(u(1))*sin(u(3)) + 6*g*l3^2*m2^2*y2*cos(u(1))*sin(u(3)) + 2*g*m2^2*x2^2*y2*cos(u(1))*sin(u(3)) - 2*I2*g*m2*x2*cos(u(1))*cos(u(3)) - 2*I2*g*l3*m2*cos(u(1))*sin(u(3)) + 2*I2*g*m2*y2*cos(u(1))*sin(u(3)) + 2*g*l3^2*m1*m2*y1*cos(u(1)) - 2*g*l1*l3^2*m1*m2*sin(u(1)) + 2*g*m1*m2*x2^2*y1*cos(u(1)) + 2*g*m1*m2*y1*y2^2*cos(u(1)) - 2*g*l1*m1*m2*x2^2*sin(u(1)) + 2*g*l3^2*m1*m2*x1*sin(u(1)) + 4*g*l1*l3*m2^2*y2*sin(u(1)) - 2*g*l1*m1*m2*y2^2*sin(u(1)) + 2*g*m1*m2*x1*x2^2*sin(u(1)) + 2*g*m1*m2*x1*y2^2*sin(u(1)) - 4*g*l3*m2^2*y2*z2*sin(u(1)) + 2*g*l1*l3^2*m2^2*cos(u(3))^2*sin(u(1)) + 2*g*l1*m2^2*y2^2*cos(u(3))^2*sin(u(1)) - 2*g*l3^2*m2^2*z2*cos(u(3))^2*sin(u(1)) - 2*g*m2^2*y2^2*z2*cos(u(3))^2*sin(u(1)) + 2*g*l1*m2^2*x2^2*sin(u(1))*sin(u(3))^2 - 2*l1^2*l3*m2^2*u(2)*u(4)*x2*sin(u(3))^2 + 4*g*l3*m2^2*y2*z2*cos(u(3))^2*sin(u(1)) - 2*m2^2*u(2)*u(4)*x2*y2*z2^2*cos(u(3))^2 + 2*l1^2*m2^2*u(2)*u(4)*x2*y2*sin(u(3))^2 - 2*l3*m2^2*u(2)*u(4)*x2*z2^2*sin(u(3))^2 + 2*m2^2*u(2)*u(4)*x2*y2*z2^2*sin(u(3))^2 + 2*l1^2*l3^2*m2^2*u(2)*u(4)*cos(u(3))*sin(u(3)) - 2*l1^2*m2^2*u(2)*u(4)*x2^2*cos(u(3))*sin(u(3)) + 2*l1^2*m2^2*u(2)*u(4)*y2^2*cos(u(3))*sin(u(3)) + 2*l3^2*m2^2*u(2)*u(4)*z2^2*cos(u(3))*sin(u(3)) - 2*m2^2*u(2)*u(4)*x2^2*z2^2*cos(u(3))*sin(u(3)) + 2*m2^2*u(2)*u(4)*y2^2*z2^2*cos(u(3))*sin(u(3)) + 4*g*l3*m2^2*x2*y2*cos(u(1))*cos(u(3)) - 2*l1*l3^2*m2^2*u(2)^2*x2*cos(2*u(3))*cos(u(3)) - 2*l1*m2^2*u(2)^2*x2*y2^2*cos(2*u(3))*cos(u(3)) + 2*l3^2*m2^2*u(2)^2*x2*z2*cos(2*u(3))*cos(u(3)) + 2*l1*l3*m2^2*u(2)^2*x2^2*cos(2*u(3))*sin(u(3)) + l1*l3*m2^2*u(2)^2*x2^2*sin(2*u(3))*cos(u(3)) - 3*l1*l3*m2^2*u(2)^2*y2^2*sin(2*u(3))*cos(u(3)) + 3*l1*l3^2*m2^2*u(2)^2*y2*sin(2*u(3))*cos(u(3)) + 2*m2^2*u(2)^2*x2*y2^2*z2*cos(2*u(3))*cos(u(3)) - 2*l1*m2^2*u(2)^2*x2^2*y2*cos(2*u(3))*sin(u(3)) - l1*m2^2*u(2)^2*x2^2*y2*sin(2*u(3))*cos(u(3)) - 2*l3*m2^2*u(2)^2*x2^2*z2*cos(2*u(3))*sin(u(3)) - l3*m2^2*u(2)^2*x2^2*z2*sin(2*u(3))*cos(u(3)) - 4*g*l3*m1*m2*y1*y2*cos(u(1)) + 3*l3*m2^2*u(2)^2*y2^2*z2*sin(2*u(3))*cos(u(3)) - 3*l3^2*m2^2*u(2)^2*y2*z2*sin(2*u(3))*cos(u(3)) + l1*l3^2*m2^2*u(2)^2*x2*sin(2*u(3))*sin(u(3)) + 4*g*l1*l3*m1*m2*y2*sin(u(1)) + 2*m2^2*u(2)^2*x2^2*y2*z2*cos(2*u(3))*sin(u(3)) + m2^2*u(2)^2*x2^2*y2*z2*sin(2*u(3))*cos(u(3)) + l1*m2^2*u(2)^2*x2*y2^2*sin(2*u(3))*sin(u(3)) - 4*g*l3*m1*m2*x1*y2*sin(u(1)) - l3^2*m2^2*u(2)^2*x2*z2*sin(2*u(3))*sin(u(3)) - m2^2*u(2)^2*x2*y2^2*z2*sin(2*u(3))*sin(u(3)) + 2*l1^2*l3*m2^2*u(2)*u(4)*x2*cos(u(3))^2 - 4*g*l1*l3*m2^2*y2*cos(u(3))^2*sin(u(1)) - 2*l1^2*m2^2*u(2)*u(4)*x2*y2*cos(u(3))^2 + 2*l3*m2^2*u(2)*u(4)*x2*z2^2*cos(u(3))^2 - 4*l1*l3*m2^2*u(2)*u(4)*x2*z2*cos(u(3))^2 + 4*l1*m2^2*u(2)*u(4)*x2*y2*z2*cos(u(3))^2 + 4*l1*l3*m2^2*u(2)*u(4)*x2*z2*sin(u(3))^2 - 4*l1*m2^2*u(2)*u(4)*x2*y2*z2*sin(u(3))^2 - 4*l1^2*l3*m2^2*u(2)*u(4)*y2*cos(u(3))*sin(u(3)) - 4*l1*l3^2*m2^2*u(2)*u(4)*z2*cos(u(3))*sin(u(3)) - 4*g*l1*l3*m2^2*x2*cos(u(3))*sin(u(1))*sin(u(3)) + 4*l1*m2^2*u(2)*u(4)*x2^2*z2*cos(u(3))*sin(u(3)) - 4*l1*m2^2*u(2)*u(4)*y2^2*z2*cos(u(3))*sin(u(3)) - 4*l3*m2^2*u(2)*u(4)*y2*z2^2*cos(u(3))*sin(u(3)) + 4*g*l1*m2^2*x2*y2*cos(u(3))*sin(u(1))*sin(u(3)) + 4*g*l3*m2^2*x2*z2*cos(u(3))*sin(u(1))*sin(u(3)) - 4*g*m2^2*x2*y2*z2*cos(u(3))*sin(u(1))*sin(u(3)) + 4*l1*l3*m2^2*u(2)^2*x2*y2*cos(2*u(3))*cos(u(3)) - 4*l3*m2^2*u(2)^2*x2*y2*z2*cos(2*u(3))*cos(u(3)) - 2*l1*l3*m2^2*u(2)^2*x2*y2*sin(2*u(3))*sin(u(3)) + 2*l3*m2^2*u(2)^2*x2*y2*z2*sin(2*u(3))*sin(u(3)) + 8*l1*l3*m2^2*u(2)*u(4)*y2*z2*cos(u(3))*sin(u(3)))/(2*(m2^2*y2^4 + I1*I2 + l3^4*m2^2 - l3^4*m2^2*cos(u(3))^2 + m2^2*x2^4*cos(u(3))^2 - m2^2*y2^4*cos(u(3))^2 - 4*l3*m2^2*y2^3 - 4*l3^3*m2^2*y2 + l1^2*l3^2*m2^2 + l1^2*m2^2*x2^2 + l3^2*m2^2*x2^2 + l1^2*m2^2*y2^2 + 6*l3^2*m2^2*y2^2 + l3^2*m2^2*z2^2 + m2^2*x2^2*y2^2 + m2^2*x2^2*z2^2 + m2^2*y2^2*z2^2 + I2*l1^2*m1 + I2*l1^2*m2 + I1*l3^2*m2 + I2*l3^2*m2 + I2*m1*x1^2 + I1*m2*x2^2 + I2*m1*y1^2 + I1*m2*y2^2 + I2*m2*y2^2 + I2*m2*z2^2 - l1^2*m2^2*y2^2*cos(u(3))^2 - 6*l3^2*m2^2*y2^2*cos(u(3))^2 - l3^2*m2^2*z2^2*cos(u(3))^2 - m2^2*y2^2*z2^2*cos(u(3))^2 - l1^2*m2^2*x2^2*sin(u(3))^2 - I2*l3^2*m2*cos(u(3))^2 - m2^2*x2^2*z2^2*sin(u(3))^2 + I2*m2*x2^2*cos(u(3))^2 - I2*m2*y2^2*cos(u(3))^2 + 4*l3*m2^2*y2^3*cos(u(3))^2 + 4*l3^3*m2^2*y2*cos(u(3))^2 + l3*m2^2*x2^3*sin(2*u(3)) + l3^3*m2^2*x2*sin(2*u(3)) - m2^2*x2*y2^3*sin(2*u(3)) - m2^2*x2^3*y2*sin(2*u(3)) + l1^2*l3^2*m1*m2 + l1^2*m1*m2*x2^2 + l3^2*m1*m2*x1^2 - 2*l1^2*l3*m2^2*y2 + l1^2*m1*m2*y2^2 + l3^2*m1*m2*y1^2 - 2*l1*l3^2*m2^2*z2 + m1*m2*x1^2*x2^2 - 2*l3*m2^2*x2^2*y2 + m1*m2*x1^2*y2^2 + m1*m2*x2^2*y1^2 - 2*l1*m2^2*x2^2*z2 + m1*m2*y1^2*y2^2 - 2*l1*m2^2*y2^2*z2 - 2*l3*m2^2*y2*z2^2 - l1^2*l3^2*m2^2*cos(u(3))^2 - 2*I2*l1*m1*x1 - 2*I1*l3*m2*y2 - 2*I2*l3*m2*y2 - 2*I2*l1*m2*z2 + 2*l1^2*l3*m2^2*y2*cos(u(3))^2 + 2*l1*l3^2*m2^2*z2*cos(u(3))^2 + 2*l1*m2^2*y2^2*z2*cos(u(3))^2 + 2*l3*m2^2*y2*z2^2*cos(u(3))^2 + 3*l3*m2^2*x2*y2^2*sin(2*u(3)) - 3*l3^2*m2^2*x2*y2*sin(2*u(3)) + 2*l1*m2^2*x2^2*z2*sin(u(3))^2 + 2*I2*l3*m2*y2*cos(u(3))^2 + I2*l3*m2*x2*sin(2*u(3)) - I2*m2*x2*y2*sin(2*u(3)) - 2*l1*l3^2*m1*m2*x1 - 2*l1^2*l3*m1*m2*y2 - 2*l1*m1*m2*x1*x2^2 - 2*l1*m1*m2*x1*y2^2 - 2*l3*m1*m2*x1^2*y2 - 2*l3*m1*m2*y1^2*y2 + 4*l1*l3*m2^2*y2*z2 + 4*l1*l3*m1*m2*x1*y2 - 4*l1*l3*m2^2*y2*z2*cos(u(3))^2 + 2*l1^2*l3*m2^2*x2*cos(u(3))*sin(u(3)) - 2*l1^2*m2^2*x2*y2*cos(u(3))*sin(u(3)) + 2*l3*m2^2*x2*z2^2*cos(u(3))*sin(u(3)) - 2*m2^2*x2*y2*z2^2*cos(u(3))*sin(u(3)) - 4*l1*l3*m2^2*x2*z2*cos(u(3))*sin(u(3)) + 4*l1*m2^2*x2*y2*z2*cos(u(3))*sin(u(3))))

f1 = q1p;
f2 = Q1pp;
f3 = q2p;
f4 = Q2pp;
F = [f1;f2;f3;f4]
X = [q1;q1p;q2;q2p]
U = [torque1;torque2]
Y = [q1;q2]
AL = jacobian(F,X)
BL = jacobian(F,U)
CL = jacobian(Y,X)
DL = jacobian(Y,U)
[u1o,u2o] = solve(f2==0,f4==0,torque1,torque2);