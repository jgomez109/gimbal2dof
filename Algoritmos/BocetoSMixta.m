%% Modelo No lineal
clear all
clc

syms Va1 Vb1 Vc1 Va2 Vb2 Vc2        % Voltajes de linea
syms ia1 ib1 ic1 ia2 ib2 ic2        % Corrientes de linea
syms te1 te2        % Ángulo eléctrico(te)
syms Te1 Tl1 Te2 Tl2        % Torque eléctrico y carga
syms R1  R2     % Iesistencia de linea
syms Kt1 Kt2        % Constante de torque [N*m/A]
syms b1 b2      % Constante de amortiguamiento (Fricción) [N*m/(Rad/s)]
syms P1  P2     % Número de polos del motor

syms u1 u2 %Indices (Ángulos eléctricos de control)

syms x1 y1 z1 % Centros de masa eslabon 1
syms x2 y2 z2 % Centros de masa eslabon 2
syms q1 q1p q1pp q2 q2p q2pp  % Posición, velocidad, aceleración eslabón 1 y 2
syms m1 m2 I1 I2 g % Masas, inercias eslabón 1 y 2; gravedad.
syms l1 l2 l3  % Distancias entre origenes de los Sistema de referencia
syms Kv1 Kv2 % Constantes para el escalado de la amplitud del voltaje en los motores
syms Vmax % Voltaje aplicado a los motores

%Relación angulo eléctrico y mecánico motor 1
qe1 = P1*q1/2 
%Relación angulo eléctrico y mecánico motor 2
qe2 = P2*q2/2

%Conversión de indice ángulo
ae1 = u1*2*pi/256;
ae2 = u2*2*pi/256;

%Voltajes Aplicados en función de la acción de control u1, Motor 1
Va1 = 0.5*Kv1*Vmax*sin(ae1)          + 0.5*Vmax
Vb1 = 0.5*Kv1*Vmax*sin(ae1 + 2*pi/3) + 0.5*Vmax
Vc1 = 0.5*Kv1*Vmax*sin(ae1 + 4*pi/3) + 0.5*Vmax

%Voltajes Aplicados en función de la acción de control u1, Motor 2
Va2 = 0.5*Kv2*Vmax*sin(ae2)          + 0.5*Vmax
Vb2 = 0.5*Kv2*Vmax*sin(ae2 + 2*pi/3) + 0.5*Vmax
Vc2 = 0.5*Kv2*Vmax*sin(ae2 + 4*pi/3) + 0.5*Vmax

%Corrientes fasoriales Motor 1
ia1 = Va1/R1
ib1 = Vb1/R1
ic1 = Vc1/R1
%Corrientes fasoriales Motor 2
ia2 = Va2/R2
ib2 = Vb2/R2
ic2 = Vc2/R2

%Torque a mover en el eslabón 1
T1 = I1*q1pp + I2*q1pp + I2*q2pp + b1*q1p + l1^2*m1*q1pp + l1^2*m2*q1pp + l3^2*m2*q1pp + m1*q1pp*x1^2 + m1*q1pp*y1^2 + m2*q1pp*y2^2 + m2*q1pp*z2^2 + g*m2*(l1*sin(q1) - z2*sin(q1) + x2*cos(q1)*cos(q2) + l3*cos(q1)*sin(q2) - y2*cos(q1)*sin(q2)) - g*m1*(y1*cos(q1) - l1*sin(q1) + x1*sin(q1)) - 2*l1*m1*q1pp*x1 - 2*l3*m2*q1pp*y2 - 2*l1*m2*q1pp*z2 - l3^2*m2*q1pp*cos(q2)^2 + m2*q1pp*x2^2*cos(q2)^2 - m2*q1pp*y2^2*cos(q2)^2 + m2*q2pp*y2*z2*cos(q2) - l1*m2*q2pp*x2*sin(q2) + m2*q2pp*x2*z2*sin(q2) + 2*l3*m2*q1pp*y2*cos(q2)^2 + l3*m2*q1pp*x2*sin(2*q2) - m2*q1pp*x2*y2*sin(2*q2) + l1*l3*m2*q2pp*cos(q2) - l1*m2*q2pp*y2*cos(q2) - l3*m2*q2pp*z2*cos(q2);
%Toque a mover en el eslabón 2
T2 = I2*q1pp + I2*q2pp + b2*q2p + l3^2*m2*q2pp + m2*q2pp*x2^2 + m2*q2pp*y2^2 - (m2*q1p*(l3^2*q1p*sin(2*q2) - q1p*x2^2*sin(2*q2) + q1p*y2^2*sin(2*q2) - 2*l1*q2p*x2*cos(q2) - 2*l1*l3*q2p*sin(q2) + 2*q2p*x2*z2*cos(q2) + 2*l1*q2p*y2*sin(q2) + 2*l3*q2p*z2*sin(q2) - 2*q2p*y2*z2*sin(q2) + 2*l3*q1p*x2*cos(2*q2) - 2*q1p*x2*y2*cos(2*q2) - 2*l3*q1p*y2*sin(2*q2)))/2 - 2*l3*m2*q2pp*y2 - g*m2*sin(q1)*(y2*cos(q2) - l3*cos(q2) + x2*sin(q2)) + m2*q1pp*y2*z2*cos(q2) - l1*m2*q1pp*x2*sin(q2) + m2*q1pp*x2*z2*sin(q2) + l1*l3*m2*q1pp*cos(q2) - l1*m2*q1pp*y2*cos(q2) - l3*m2*q1pp*z2*cos(q2);

%Dinámica mecánica (Balance de Torques en el eje del motor 1)
ecu5 = T1 ==  Kt1*( sin(qe1)*ia1 + sin(qe1 + 2*pi/3)*ib1 + sin(qe1 + 4*pi/3)*ic1 )
%Dinámica mecánica (Balance de Torques en el eje del motor 2)
ecu6 = T2 ==  Kt2*( sin(qe2)*ia2 + sin(qe2 + 2*pi/3)*ib2 + sin(qe2 + 4*pi/3)*ic2 )

%Despejar aceleraciones angulares
[q1pp,q2pp] = solve(ecu5,ecu6,q1pp,q2pp);
q1pp = simplify(q1pp)       % Razón de cambio q1p
q2pp = simplify(q2pp)       % Razón de cambio q2p

x = [q1 ; q1p ; q2 ; q2p]       % ESTADOS
X = [q1p; q1pp; q2p; q2pp]      % DINÁMICAS
u = [u1; u2]        % ENTRADAS DE CONTROL 
y = [q1; q2]        % SALIDAS

A = jacobian(X,x)       % (dX/dx)
B = jacobian(X,u)       % (dX/du)
% C = jacobian(y,x)     % (dy/dx) (Radianes)
C = [1*180/pi 0 0 0     % (dy/dx) (Grados)
     0        0 180/pi 0]
D = jacobian(y,u)       % (dy/du)

%% Modelo Lineal
%Razones de cambio igual a cero
q1p = 0
q2p = 0
%Punto de equilibrio
u1 =   512  
u2 =   -384 
q1 =   (-90)*pi/180
q2 =   ( 90)*pi/180

%Valores de los parámetros
R1 = 15.445
R2 =  15.445
P1 = 14
P2  =14
Kv1 = 126.16/255
Kv2 = 112.88/255
x1 =-0.011
y1 =0
z1 =0.0047
x2 =0.00065
y2 =0.018
z2 =0.031
l1 =0.01625
l2 =0.03475
l3 =0.02275
g  =9.81
Vmax = 12
Kt1 = 0.11
b1 =0.0007
Kt2 = 0.11
b2 =0.0007
m1 =0.062  
m2 =0.0777
I1 =0.00005182988
I2 =0.00001912189

%Modelo lineal en espacio de estados
AL = double(subs(A))
BL = double(subs(B))
CL = double(subs(C))
DL = double(subs(D))
sys = ss(AL,BL,CL,DL)
%Modelo lineal en espacio de estados discreto
sysd = c2d(sys,0.001,'zoh')
%Comparación de la respuesta continúa y discreta
step(sys,sysd)
%Matrices A y B discretizadas
GL = sysd.a
HL = sysd.b

%% Definción de entradas, salidas y estados:
disp(' ')
disp('Número de Entradas:')
p = 2
disp('Número de Salidas:')
r = 2  %Número de salidas
disp('Número de estados:')
n = 4
s = zpk('s')
%% Diseño controlador de sensibilidad mixta H∞
%Funciones de ponderación
W1 = 100    * (s/150+1) /(s/0.35+1)  % Filtro de pasabajos Error
W2 = 0.001 * (s/100+1) /(s/10000 +1) % Filtro de pasaaltos Acción de control
W3 = 0.00001* (s/1+1)/(s/10000+1)  % Filtro de pasaaltos Variable controlada

% W1 = makeweight(500000,[200 10],5);  % Filtro de pasabajos Error
% W2 = makeweight(0.01,[5 5.5],500); % Filtro de pasaaltos Acción de control
% W3 = makeweight(0.05,[1 5],500); % Filtro de pasaaltos Variable controlada

%Bode de las funciones de ponderación
figure(1)
subplot(1,3,1)
bodemag(W1)
grid
subplot(1,3,2)
bodemag(W2)
grid
subplot(1,3,3)
bodemag(W3)
grid

%Diseño del controlador
[K,CL,GAM] = mixsyn(sys,W1,W2,W3);
% [K,CL,GAM] = mixsyn(sys,W1,[],W3);
% [K,CL,GAM] = mixsyn(sys,W1,W2,[]);

% Lazo cerrado de las funciones de sensibilidad
S = feedback(eye(2),sys*K) % Error
T = feedback(sys*K,eye(2)) % Yi
R = feedback(K,sys) % Ui

%Comparación del desempeño del controlador 
figure(2)
sigma(S,'r',GAM/W1,'b')
title('Error')
legend('S','GAM/W1')
grid
figure(3)
sigma(R,'r',GAM/W2,'b')
title('Acción de control')
legend('R','GAM/W2')
grid
figure(4)
sigma(T,'r',GAM/W3,'b')
title('Variable controlada')
legend('T','GAM/W3')
grid
%Respuesta en lazo cerrado
figure(5)
step(T)
title('Yi')
grid
figure(6)
step(R)
title('Ui')
grid
K % Controlador
% S = error
% R = Acción de control
% T = Salida
%% Discretización del controlador de sensibilidad mixta
Tm = 0.001
Kd = c2d(K,Tm)
T = feedback(sys*K,eye(r))
Tz = feedback(sysd*Kd,eye(r))
U = feedback(K,sys)
Uz = feedback(Kd,sysd)
figure(5)
step(T,Tz)
title('Yi')
legend('Sin Reducir Continua','Sin Reducir Discreta')
grid
figure(6)
step(U,Uz)
title('Ui')
legend('Sin Reducir Continua','Sin Reducir Discreta')
grid

%Impresión de las matrices del controlador en espacio de estados discreto
fprintf('Ac:\n')
for i=1: size(Kd.a,1)
fprintf('%.10f,\t',Kd.a(i,:)); fprintf('\n')
end
fprintf('\n')

fprintf('Bc:\n')
for i=1: size(Kd.b,1)
fprintf('%.10f,\t',Kd.b(1,:)); fprintf('\n')
end
fprintf('\n')

fprintf('Cc:\n')
for i=1: size(Kd.c,1)
fprintf('%.10f,\t',Kd.c(1,:)); fprintf('\n')
end
fprintf('\n')

fprintf('Dc:\n')
for i=1: size(Kd.d,1)
fprintf('%.10f,\t',Kd.d(1,:)); fprintf('\n')
end
fprintf('\n')
%% Reducir controlador de sensibilidad mixta
% hankelsv(K)
% ksv = hankelsv(K)
Kr = reduce(K,8)
T = feedback(sys*K,eye(r))
Tr = feedback(sys*Kr,eye(r))
U = feedback(K,sys)
Ur = feedback(Kr,sys)
figure(5)
step(T,Tr)
title('Yi')
legend('Sin reducir Continua','Reducido Continua')
grid
figure(6)
step(U,Ur)
title('Ui')
legend('Sin reducir Continua','Reducido Continua')
grid
Kr
%% Discretización del controlador de sensibilidad mixta reducido
Tm = 0.001
Kd = c2d(Kr,Tm)
% Kd = c2d(Kr,Tm,'tustin')
% sysd = c2d(sys,Tm)
T = feedback(sys*Kr,eye(r))
Tz = feedback(sysd*Kd,eye(r))
U = feedback(Kr,sys)
Uz = feedback(Kd,sysd)
figure(5)
T.InputName = {'e1','e2'}
T.OutputName = {'Eslabón 1', 'Eslabón 2'}
step(T,Tz)
title('H_∞  Yi')
legend('Continuo','Discreto')
grid
figure(6)
U.InputName = {'e1','e2'}
U.OutputName = {'u1','u2'}
step(U,Uz)
title('H_∞  Ui')
legend('Continuo','Discreto')
grid

%Impresión de las matrices del controlador en espacio de estados discreto
fprintf('Ac:\n')
for i=1: size(Kd.a,1)
fprintf('%.15f,\t',Kd.a(i,:)); fprintf('\n')
end
fprintf('\n')

fprintf('Bc:\n')
for i=1: size(Kd.b,1)
fprintf('%.15f,\t',Kd.b(i,:)); fprintf('\n')
end
fprintf('\n')

fprintf('Cc:\n')
for i=1: size(Kd.c,1)
fprintf('%.15f,\t',Kd.c(i,:)); fprintf('\n')
end
fprintf('\n')

fprintf('Dc:\n')
for i=1: size(Kd.d,1)
fprintf('%.15f,\t',Kd.d(i,:)); fprintf('\n')
end
fprintf('\n')