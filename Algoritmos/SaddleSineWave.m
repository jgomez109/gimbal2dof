%% THIRD HARMONIC SINE WAVE
t = linspace(0,1,100);
sinePure = sin(2*pi*t);
sinePure_ThirdA = sin(3*2*pi*t);

figure(10)
subplot(2,1,1); plot(t.*360,sinePure,t.*360,sinePure_ThirdA); xlim([0 360])
subplot(2,1,2); plot(t.*360,sinePure + sinePure_ThirdA);xlim([0 360]) 

figure(11)
plot(t.*360, sin(2*pi*t + 120*pi/180));xlim([0 360]) 

%% SADDLE SINE
clear all
clc
t = linspace(0,1,256);
sinePure = sin(2*pi*t);
i_120 = find(t==120/360);
sin120 = sinePure(1:i_120);
sin120_ = sin120(end:-1:1);
sin120_(1) = [];
sinSaddle = [sin120, sin120_, zeros(1,length(t) - i_120*2 + 1)];
sinSaddle120 = circshift(sinSaddle,85);
sinSaddle240 = circshift(sinSaddle,170);

figure(10)
subplot(2,1,1); stem(t.*360,sinePure); xlim([0 360])
subplot(2,1,2); stem(t.*360,sinSaddle); xlim([0 360])
figure(11)
subplot(4,1,1); plot(t.*360,sinSaddle,t.*360,sinSaddle120,'r',t.*360,sinSaddle240,'k'); legend('A', 'B', 'C'); xlim([0 360])
subplot(4,1,2); plot(t.*360,sinSaddle); xlim([0 360])
subplot(4,1,3); plot(t.*360,sinSaddle120,'r'); xlim([0 360])
subplot(4,1,4); plot(t.*360,sinSaddle240,'k'); xlim([0 360])
% Vector para el algoritmo:
sinSaddleVector = round(sinSaddle.*255);
figure(12)
plot(t.*360,sinSaddleVector)

%% RMS VALUE PWM: 
clear all
clc
Vp1 = 100/255
syms  Vp2 k 

f1 = 127*Vp1*sin(2*pi*k/256) + 128; a1 = 0; b1 = 256;
f2 = 255*Vp2*sin(2*pi*k/256) - 1;   a2 = 0; b2 = 120*256/360;
T = 256;
rms1 = sqrt((1/T)*int(f1^2,k,a1,b1));
rms1 = vpa(simplify(rms1),3);
rms2 = 2*sqrt((1/T)*int(f2^2,k,a2,b2));
rms2 = vpa(simplify(2*rms2),3);
ecu = rms1 == rms2;
[sol,par,con] = solve(ecu,Vp2,'ReturnConditions',true)
% Conclusión: Un MaxPWM de 39.2 % en PURE SineWave es equivalente a 29.5 % de
% SADDLE SineWave.