#include <Arduino.h>
#include <Wire.h>


// Variables de MOTORES:
//--- --- --- --- ---
#define frecuenciaPWM 20000 // Frecuencia de trabajo PWM
#define resolucionPWM 8 // Resolución del PWM 
#define canalPWM0 0 // Canal 0 PWM
#define canalPWM1 1 // Canal 1 PWM
#define canalPWM2 2 // Canal 2 PWM
#define canalPWM3 3 // Canal 3 PWM
#define canalPWM4 4 // Canal 4 PWM
#define canalPWM5 5 // Canal 5 PWM
#define EN1_motor2 27  // Pin
#define EN2_motor2 13  // Pin
#define EN3_motor2 25  // Pin
#define IN1_motor2 14  // Pin   
#define IN2_motor2 12  // Pin  
#define IN3_motor2 26  // Pin
#define EN1_motor1 19  // Pin
#define EN2_motor1 23  // Pin
#define EN3_motor1 18  // Pin
#define IN1_motor1 15  // Pin   
#define IN2_motor1 4   // Pin 
#define IN3_motor1 5   // Pin
#define N_SIN 256 // cantidad de muestras de la señal senoidal
bool pwmWave = 0; // 1: PURE, 0 SADDLE 
int pwmSinWave[256]; 
int pwmSinWaveSaddle [256] ={0,6,13,19,25,31,38,44,50,56,62,68,74,80,86,92,98,104,109,115,121,126,132,137,142,147,152,157,162,167,172,176,181,185,190,194,198,202,205,209,213,216,219,222,225,228,231,234,236,238,241,243,244,246,248,249,250,251,252,253,254,254,255,255,255,255,255,254,254,253,252,251,250,248,247,245,243,242,239,237,235,232,230,227,224,221,224,227,230,232,235,237,239,242,243,245,247,248,250,251,252,253,254,254,255,255,255,255,255,254,254,253,252,251,250,249,248,246,244,243,241,238,236,234,231,228,225,222,219,216,213,209,205,202,198,194,190,185,181,176,172,167,162,157,152,147,142,137,132,126,121,115,109,104,98,92,86,80,74,68,62,56,50,44,38,31,25,19,13,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
static uint8_t pwm_a_motor2 ;
static uint8_t pwm_b_motor2 ;
static uint8_t pwm_c_motor2 ;
static uint8_t pwm_a_motor1 ;
static uint8_t pwm_b_motor1 ;
static uint8_t pwm_c_motor1 ;

// Variables de TIEMPO:
//--- --- --- --- ---
#include "driver/timer.h"
#define tm 1.0f //ms
static intr_handle_t s_timer_handle;
static long long int tTimer, tTimerInicial, tTimerActual, ticks, tickActual, tickInicial;
static int countSensor, countControl, countPrint, countGimbal;
static long long int countSec;
static boolean flagTimer;


// Variables de SENSOR:
//--- --- --- --- ---
#include <BasicLinearAlgebra.h>
#define MPU 0x68
#define rawaScale  0.000061035f // 1/16384
#define rawgScale  0.00013323f // (1/131)*pi/180
//#define rawgScale  0.0076336f // (1/131)
#define rawaxBias  -918
#define rawayBias  116
#define rawazBias  -916
#define mx 0.000063415f
#define bx -0.1066f
#define my 0.000063959f
#define by -0.0398f
#define mz 0.000057231f
#define bz -0.0262f
static bool LED;
static int countLED;
const int16_t n = 5000; // Muestras a tomar en el la calibración del Gyro
static int16_t rawgx, rawgy, rawgz; static int16_t rawax, raway, rawaz; 
float rawgxSum,  rawgySum,  rawgzSum;
float rawgxBias, rawgyBias, rawgzBias;
static float ax, ay, az;
static float wx, wy;
static float dt = tm*0.001;
static float roll, pitch;
static float roll_acc, pitch_acc;  
const int nf = 3;
static float sumWxv[n], sumWyv[n];
// KALMAN:

BLA::Matrix<4,4>  A;
BLA::Matrix<4,2>  B;
BLA::Matrix<2,4>  C = { 1, 0, 0, 0,
                        0, 1, 0, 0,};                          
BLA::Matrix<2,1>  u, yHat, Z;   
BLA::Matrix<4,1>  xHatBar,xHat;   
BLA::Matrix<4,4>  p = { 1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,         
                        0, 0, 0, 1,}; 
BLA::Matrix<4,4>  pBar;     
const float Qgx  = 0.008*0.008; // 0.001
const float Qgy  = 0.008*0.008;
const float Qbgx = 0.007*0.007*dt*dt; // 0.003
const float Qbgy = 0.074*0.074*dt*dt;  
BLA::Matrix<4,4>  Q = { Qgx, 0  , 0   , 0   ,
                        0,   Qgy, 0   , 0   ,
                        0,   0  , Qbgx, 0   ,         
                        0,   0  , 0   , Qbgy,};     
const float Rax = 3.0; // 0.03
const float Ray = 2.2;  
BLA::Matrix<2,2> R = {Rax, 0  , 
                      0  , Ray,};    
BLA::Matrix<4,2> K;                                           
BLA::Matrix<2,2> tmpK;                                     
BLA::Matrix<4,4>  I4 = {1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,         
                        0, 0, 0, 1,};   
float rollGyro, pitchGyro, sumWx, sumWy;                   
                                                                 
// Variables CONTROL:
//--- --- --- --- ---
static int32_t u1, u2, u1o, u2o;
/* Control con Valores x1000 */

// static int32_t DTms = 1, DTinv= 1000;
// static int32_t rollSetPoint,  rollErrorSum,  rollErrorOld,   rollkp  = 2000, rollki  = 10, rollkd  = 16;
//static int32_t pitchSetPoint, pitchErrorSum, pitchErrorOld,  pitchkp = 140, pitchki = 15, pitchkd =2; // Fc 
//static int32_t pitchPIDval;

/* Control con Valores x1*/
//******************* pitchkp = 80, pitchki = 7, pitchkd = 7.5, pitchkw = 10; Saddle: 95,85; Kalaman: R = 3, 2.2

static float pitchSetPoint, pitchErrorSum, pitchErrorOld, pitchdu, pitchkp = 75, pitchki = 6.0, pitchkd = 7, pitchkw = 10; // 
static float pitchPIDval;
static float rollSetPoint, rollErrorSum, rollErrorOld, rolldu, rollkp = 25, rollki = 5.0, rollkd = 17, rollkw = 10; // 
static float rollPIDval;

static float maxLimPID = 10000;

static int countSetPoint;   

static long long int timeSetPoint;    
static bool setPointFlag = 0; // 1: Set Point Variable en secuencia pre definida, 0: Set Point = 0 
static bool setPointSecStart;   
static float k;  
static int16_t rawgx_1, rawgy_1;

// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°           
/* FUNCIONES ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*/
// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°  


//********************
//Funciones de TIEMPO:
static void IRAM_ATTR timer_tg1_isr(void* arg) // CORE 1
{	//Reset irq and set for next time
    TIMERG1.int_clr_timers.t1 = 1;
    TIMERG1.hw_timer[1].config.alarm_en = 1;
    flagTimer = 1;
    tTimerActual = micros();
}
void timer_tg1_initialise (int timer_period_us)
{           timer_config_t config = {
            .alarm_en = true,				//Alarm Enable
            .counter_en = false,			//If the counter is enabled it will start incrementing / decrementing immediately after calling timer_init()
            .intr_type = TIMER_INTR_LEVEL,	//Is interrupt is triggered on timer’s alarm (timer_intr_mode_t)
            .counter_dir = TIMER_COUNT_UP,	//Does counter increment or decrement (timer_count_dir_t)
            .auto_reload = true,			//If counter should auto_reload a specific initial value on the timer’s alarm, or continue incrementing or decrementing.
            .divider = 80   				//Divisor of the incoming 80 MHz (12.5nS) APB_CLK clock. E.g. 80 = 1uS per timer tick
    };

    timer_init(TIMER_GROUP_1, TIMER_1, &config);
    timer_set_counter_value(TIMER_GROUP_1, TIMER_1, 0);
    timer_set_alarm_value(TIMER_GROUP_1, TIMER_1, timer_period_us);
    timer_enable_intr(TIMER_GROUP_1, TIMER_1);
    timer_isr_register(TIMER_GROUP_1, TIMER_1, &timer_tg1_isr, NULL, 0, &s_timer_handle);
    timer_start(TIMER_GROUP_1, TIMER_1);
}


//********************
//Funciones de IMU:
inline static void updateAttitude() __attribute__((always_inline));
// inline void LPF() __attribute__((always_inline));
static void initCalIMU() {  // CORE 1
            Serial.println(" ");
            Serial.print("ATENCION! ");
            Serial.println("NO MOVER LA IMU");
            Serial.println("Calibrando....");
            Wire.begin(21,22); // A4=SDA / A5=SCL 
            Wire.setClock(1000000);
            //Wire.setClock(400000);  
            Wire.beginTransmission(MPU);
            Wire.write(0x6B);
            Wire.write(0);
            Wire.endTransmission();
            vTaskDelay(2000);
            for (int i = 0; i < n; i++) {
              //Leer los valores del Giroscopo
                  Wire.beginTransmission(MPU); 
                  Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
                  Wire.endTransmission(false);
                  Wire.requestFrom(MPU,14);   //A partir del 0x3B, se piden 12 registros
                  rawax = Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
                  raway = Wire.read()<<8|Wire.read();
                  rawaz = Wire.read()<<8|Wire.read();
                  Wire.read();
                  Wire.read();
                  rawgx = Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
                  rawgy = Wire.read()<<8|Wire.read();  
                  rawgz = Wire.read()<<8|Wire.read();   

              rawgxSum += rawgx ;
              rawgySum += rawgy;    
              rawgzSum += rawgz;      
            }

            rawgxBias = rawgxSum / float(n);
            rawgyBias = rawgySum / float(n);
            rawgzBias = rawgzSum / float(n); 

            Serial.println("Offsets Calculados");
            Serial.println("--------------------");
            Serial.println("Gyro [ Dec ]"); 
            Serial.print("Gyro X: "); Serial.print(rawgxBias); Serial.print(" ");
            Serial.print("Gyro Y: "); Serial.print(rawgyBias); Serial.print(" ");  
            Serial.print("Gyro Z: "); Serial.println(rawgzBias);
            Serial.println("--------------------");  
          //  for (int i = 0; i < muestrasCalibracion; i++) {
          //   Serial.print(muestrasGyro[0][i]); Serial.print(" ");
          //   Serial.println(muestrasGyro[1][i]);
          //  }
            Serial.println("--------------------"); Serial.println(" ");
            Serial.print("El programa inicia en 1 segundo en MODO:");
            if ( pwmWave == 1) Serial.println("'PURE SINE'...."); else Serial.println("'SADDLE SINE'....");
            Serial.println(" ");
            Serial.println("----------- ENJOY -----------  ");  
            Serial.println(" ");
            vTaskDelay(1000);
}
static void leerIMU(){ // CORE 1
            Wire.beginTransmission(MPU); Wire.write(0x3B); Wire.endTransmission(false); Wire.requestFrom(MPU,12);
            rawax = Wire.read()<<8|Wire.read(); 
            raway = Wire.read()<<8|Wire.read();
            rawaz = Wire.read()<<8|Wire.read();
            Wire.read();
            Wire.read();
            rawgx = Wire.read()<<8|Wire.read(); 
            rawgx_1 = rawgx;
            rawgy = Wire.read()<<8|Wire.read();  
            rawgy_1 = rawgy;
} 
// void LPF(float * q, float i, float coeff) { // CORE 1
//         *q += (i - *q) * coeff; 
// }
static void updateAttitude(){ // CORE 1 

            ax = -rawax*mx + bx + 0.18;
            ay = -raway*my + by ;
            az = -rawaz*mz + bz + 0.05;      
            
            sumWx = 0;
            sumWy = 0;
            for (int i = 0; i< nf; i++){                   
                sumWx += rawgx - rawgxBias ;                
                sumWy += rawgy - rawgyBias ;  
                sumWxv[i] = sumWx;
                sumWyv[i] = sumWy;       
            }           
            wx = sumWx*rawgScale/float(nf);
            wy = sumWy*rawgScale/float(nf);
            // wx = (rawgx - rawgxBias)*rawgScale;                
            // wy = (rawgy - rawgyBias)*rawgScale ;  



// KALMAN:
            u = {wx, wy};
            A = {
                1, 0, -dt,  0 ,
                0, 1,  0 , -dt,
                0, 0,  1 ,  0 ,
                0, 0,  0 ,  1 ,
            };
            B = {
                dt, 0 ,
                0 , dt, 
                0 , 0 ,
                0 , 0 ,
            };            
            xHatBar = A*xHat + B*u;
            yHat = C*xHat;
            pBar = A*p*(~A) + Q;
            tmpK = C*pBar*(~C) + R;
            K = pBar*(~C)*tmpK.Inverse();
                 
            Z = {  
                   atan2((ay),  sqrt( ax*ax + az*az) ),
                  -atan2((ax),  sqrt( ay*ay + az*az) ),
                };
                                    
            xHat = xHatBar + K*(Z - yHat);
            p = (I4 - K*C)*pBar;
            roll =  xHat(0)*RAD_TO_DEG ; // +2
            pitch = xHat(1)*RAD_TO_DEG ; //- 4
            rollGyro  += wx*dt;
            pitchGyro += wy*dt;            
            roll_acc  =  Z(0)*RAD_TO_DEG ; // +2
            pitch_acc =  Z(1)*RAD_TO_DEG ; //- 4

}

//********************
//Funciones de MOTORES:
inline void MoveMotorPosSpeed() __attribute__((always_inline));
void initMotors() { //CORE 1
if ( pwmWave == 1){
    pwm_a_motor2 = 128;
    pwm_b_motor2 = 128;
    pwm_c_motor2 = 128;
    pwm_a_motor1 = 128;
    pwm_b_motor1 = 128;
    pwm_c_motor1 = 128;
    for (int i = 0; i < N_SIN; i++) // Array
    {
        pwmSinWave[i] =  sin(2.0 * i / N_SIN * 3.14159265) * 127.0; // Calculo onda senoidal
    }
}else{
       for (int i = 0; i < N_SIN; i++) // Array
    {
        pwmSinWave[i] =  pwmSinWaveSaddle[i]; 
    } 
}


 
    ledcSetup(canalPWM0, frecuenciaPWM, resolucionPWM); //Asigno los parámetros PWM al canal 0 PWM
    ledcSetup(canalPWM1, frecuenciaPWM, resolucionPWM); //Asigno los parámetros PWM al canal 1 PWM
    ledcSetup(canalPWM2, frecuenciaPWM, resolucionPWM); //Asigno los parámetros PWM al canal 2 PWM
    ledcSetup(canalPWM3, frecuenciaPWM, resolucionPWM); //Asigno los parámetros PWM al canal 3 PWM
    ledcSetup(canalPWM4, frecuenciaPWM, resolucionPWM); //Asigno los parámetros PWM al canal 4 PWM
    ledcSetup(canalPWM5, frecuenciaPWM, resolucionPWM); //Asigno los parámetros PWM al canal 5 PWM
    ledcAttachPin(IN1_motor1, canalPWM0); //Asigno el canal 0 PWM al pin
    ledcAttachPin(IN2_motor1, canalPWM1); //Asigno el canal 1 PWM al pin
    ledcAttachPin(IN3_motor1, canalPWM2); //Asigno el canal 2 PWM al pin
    ledcAttachPin(IN1_motor2, canalPWM3); //Asigno el canal 0 PWM al pin
    ledcAttachPin(IN2_motor2, canalPWM4); //Asigno el canal 1 PWM al pin
    ledcAttachPin(IN3_motor2, canalPWM5); //Asigno el canal 2 PWM al pin
    pinMode(EN1_motor1, OUTPUT);
    pinMode(EN2_motor1, OUTPUT);
    pinMode(EN3_motor1, OUTPUT);
    pinMode(EN1_motor2, OUTPUT);
    pinMode(EN2_motor2, OUTPUT);
    pinMode(EN3_motor2, OUTPUT);
    ledcWrite(canalPWM0, 255); // Escribo el valor PWM para la fase A motor 1
    ledcWrite(canalPWM1, 255); // Escribo el valor PWM para la fase B motor 1
    ledcWrite(canalPWM2, 255); // Escribo el valor PWM para la fase C motor 1
    ledcWrite(canalPWM3, 255); // Escribo el valor PWM para la fase A motor 2
    ledcWrite(canalPWM4, 255); // Escribo el valor PWM para la fase B motor 2
    ledcWrite(canalPWM5, 255); // Escribo el valor PWM para la fase C motor 2
    digitalWrite(EN1_motor1, HIGH);
    digitalWrite(EN2_motor1, HIGH);
    digitalWrite(EN3_motor1, HIGH);
    digitalWrite(EN1_motor2, HIGH);
    digitalWrite(EN2_motor2, HIGH);
    digitalWrite(EN3_motor2, HIGH);
}
void MoveMotorPosSpeed(uint8_t motorNumber, int MotorPos, uint16_t maxPWM) //CORE 1
{
    uint16_t posStep;
    uint16_t pwm_a;
    uint16_t pwm_b;
    uint16_t pwm_c;

    // fetch pwm from sinus table
    posStep = MotorPos & 0xff;
    pwm_a = pwmSinWave[(uint8_t)posStep];
    pwm_b = pwmSinWave[(uint8_t)(posStep + 85)];
    pwm_c = pwmSinWave[(uint8_t)(posStep + 170)];

    // apply power factor
    pwm_a = maxPWM * pwm_a;
    pwm_a = pwm_a >> 8;
    // pwm_a += 128;

    pwm_b = maxPWM * pwm_b;
    pwm_b = pwm_b >> 8;
    // pwm_b += 128;

    pwm_c = maxPWM * pwm_c;
    pwm_c = pwm_c >> 8;
    // pwm_c += 128;
    if(pwmWave == 1){
       pwm_a += 128; 
       pwm_b += 128;   
       pwm_c += 128;
    }

    // set motor pwm variables
    if (motorNumber == 2)
    {
        pwm_a_motor2 = (uint8_t)pwm_a;
        pwm_b_motor2 = (uint8_t)pwm_b;
        pwm_c_motor2 = (uint8_t)pwm_c;
        ledcWrite(canalPWM3, pwm_a_motor2); // Escribo el valor PWM para la fase A motor 2
        ledcWrite(canalPWM4, pwm_b_motor2); // Escribo el valor PWM para la fase B motor 2
        ledcWrite(canalPWM5, pwm_c_motor2); // Escribo el valor PWM para la fase C motor 2
    }

    if (motorNumber == 1)
    {
        pwm_a_motor1 = (uint8_t)pwm_a;
        pwm_b_motor1 = (uint8_t)pwm_b;
        pwm_c_motor1 = (uint8_t)pwm_c;   
        ledcWrite(canalPWM0, pwm_a_motor1); // Escribo el valor PWM para la fase A motor 1
        ledcWrite(canalPWM1, pwm_b_motor1); // Escribo el valor PWM para la fase B motor 1
        ledcWrite(canalPWM2, pwm_c_motor1); // Escribo el valor PWM para la fase C motor 1   
    }
}

float ComputePID(float in, float setPoint, float *errorSum, float *errorOld, float *du,float kp, float ki, float kd, float kw){
                float error = setPoint - in;
                float up, ui, ud, uPID, uLim;
                up = kp*error;
                *errorSum +=  error - kw * *du;  
                ui = ki * *errorSum;
                ud = kd*(error - *errorOld);     
                uPID = up + ui + ud;
                if ( uPID > maxLimPID)  uLim = maxLimPID;
                if ( -maxLimPID <= uPID && uPID <= maxLimPID) uLim = uPID;
                if ( uPID < -maxLimPID) uLim = maxLimPID; 
                *errorOld = error;        
                *du = uPID - uLim;

                return uLim;
}
/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ÁREA DE TRABAJO ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */

// ° CORE 1: IMPRESIÓN:
static void printTask(void*)  // CORE 1 
{static TickType_t xLastWakeTime = xTaskGetTickCount();
    while (1) {
      switch (countPrint){
            case 0:{
                    
            }break;
            case 1:{

                    printf("%.2f, ",roll_acc);                            
                    printf("%.2f, ",roll);                                               
                    printf("%.2f, ",pitch_acc);  
                    printf("%.2f, ",pitch);                                         
                    printf("%d, "  ,u1); 
                    printf("%d\n"  ,u2); 

            }break;

      }

    vTaskDelayUntil(&xLastWakeTime, 5); 
    }
}

// ° CORE 0: CONTROL:
static void control( void * pvParameters ) // CORE 0
{  static TickType_t xLastWakeTime = xTaskGetTickCount(); 
   static int tmSetPoint = tm;
    while(1)
    {   tickActual = xTaskGetTickCount();
        switch(countGimbal){
              case 0:{
                      countSensor++;
                      countPrint++;
                      countGimbal++;
                      tickInicial = tickActual;
                      xTaskCreate(printTask, "printTask", 2 * 1024, nullptr, 5, nullptr); 
                      if ( setPointFlag == 1) { // 1: Inicia con Secuencia de Set Point
                            countSetPoint = 0; 
                      } else {
                            countSetPoint = -1; 
                            setPointSecStart = 0;
                      }                         
              }break;

              case 1:{
                      ticks = tickActual-tickInicial;
                      updateAttitude();
                      switch(countControl){

                            case 0:{const int tWait  = 2000;
                                    countSec++;
                                    if (countSec >= tWait) {
                                        countSec = 0;
                                        countControl++;           
                                    }
                            }break;

                            case 1:{const int tWait  = 25;
                                    countSec++;
                                    if (countSec >= tWait) {
                                        countSec = 0;
                                        if ( -0.1 < pitch && pitch < 0.1){
                                            u1o = u1;
                                            countControl++;
                                        }else{
                                            if ( pitch > 0) u1--; else u1++;
                                        }
                                    }

                            }break;
                            case 2:{const int tWait  = 25;
                                    countSec++;
                                    if (countSec >= tWait) {
                                        countSec = 0;
                                        if ( -0.1 < roll && roll < 0.1){
                                            u2o = u2;
                                            countControl = 20;
                                        }else{
                                            if ( roll > 0) u2--; else u2++;
                                        }
                                    }

                            }break;  

                            case 20:{const int tWait  = 3000;
                                    countSec++;
                                    if (countSec >= tWait) {
                                        countSec = 0;
                                        countControl = 3;           
                                    }
                            }break;

                            case 3:{ // CONTROLADOR
                                    countLED++;
                                    if(countLED >= 100){
                                        countLED = 0;
                                        if (LED == 1) {
                                            LED = 0; GPIO.out_w1ts = (1 << 2);
                                         } else{
                                            LED = 1; GPIO.out_w1tc = (1 << 2);
                                         }
                                    } 
pitchPIDval = ComputePID(pitch, pitchSetPoint, &pitchErrorSum, &pitchErrorOld, &pitchdu, pitchkp,pitchki, pitchkd, pitchkw);
rollPIDval = ComputePID(roll, rollSetPoint, &rollErrorSum, &rollErrorOld, &rolldu, rollkp,rollki, rollkd, rollkw);   

u1 = u1o + pitchPIDval;
u2 = u2o + rollPIDval; 

setPointSecStart = 1;   
                              
                            }break;                               


                      }
                      // Amplitud (maxPWM): PURE ~ 1.328*SADDLE; Mayor Torque (Calor) en el Saddle para un mismo valor de Amplitud 
                      if( pwmWave == 1){
                        MoveMotorPosSpeed(1,u1,133);
                        MoveMotorPosSpeed(2, u2,126);
                      }else{
                        MoveMotorPosSpeed(1,u1,95);
                        MoveMotorPosSpeed(2, u2,85);                          
                      }

              }break;     
        }
         if ( setPointSecStart == 1) {
            switch (countSetPoint)
            {

                case 0:{
                        const int tWait = 5000;
                        timeSetPoint += tmSetPoint;
                        rollSetPoint = 0;
                        pitchSetPoint = 0;                                        
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            countSetPoint++;                                 
                        }
                }break;
              
                case 1:{ // SUBIR ROLL
                        const int tWait = 10;
                        const float tRamp = 3000.0/tWait; 
                        const float angle = 30.0;                                       
                        timeSetPoint += tmSetPoint;                               
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            k++;
                            rollSetPoint = k*(angle/tRamp);
                            if (rollSetPoint >= angle){
                                rollSetPoint = angle;
                                countSetPoint++;
                                k = 0;   
                            }

                        }
                }break;   

                case 2:{// ESPERAR tWait
                        const int tWait = 1000;
                        timeSetPoint += tmSetPoint;                                  
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            countSetPoint++;                                             
                        }
                }break;  

                case 3:{ // BAJAR ROLL
                        const int tWait = 10;
                        const float tRamp = 3000.0/tWait; 
                        const float angle = -30.0;                                       
                        timeSetPoint += tmSetPoint;                              
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            k++;
                                rollSetPoint = -angle + k*(angle/tRamp);
                            if (rollSetPoint <= 0){
                                rollSetPoint = 0;
                                countSetPoint++;
                                k = 0;   
                            }

                        }   
                }break;   

                case 4:{// ESPERAR tWait
                        const int tWait = 1000;
                        timeSetPoint += tmSetPoint;                                  
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            countSetPoint++;                                             
                        }
                }break;  
               case 5:{// BAJAR ROLL, SEGUNDA VEZ
                        const int tWait = 10;
                        const float tRamp = 3000.0/tWait; 
                        const float angle = -30.0;                                       
                        timeSetPoint += tmSetPoint;                               
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            k++;
                            rollSetPoint = k*(angle/tRamp);
                            if (rollSetPoint <= angle){
                                rollSetPoint = angle;
                                countSetPoint++;
                                k = 0;   
                            }

                        }
                }break;   

                case 6:{// ESPERAR tWait
                        const int tWait = 1000;
                        timeSetPoint += tmSetPoint;                                  
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            countSetPoint++;                                             
                        }
                }break;  

                case 7:{// SUBIR ROLL, SEGUNDA VEZ
                        const int tWait = 10;
                        const float tRamp = 3000.0/tWait; 
                        const float angle = 30;                                       
                        timeSetPoint += tmSetPoint;                              
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            k++;
                                rollSetPoint = -angle + k*(angle/tRamp);
                            if (rollSetPoint >= 0){
                                rollSetPoint = 0;                                
                                countSetPoint++;
                                k = 0;   
                            }

                        }   
                }break;  

               case 8:{
                        const int tWait = 2000;
                        timeSetPoint += tmSetPoint;                               
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            rollSetPoint = -3;
                            pitchSetPoint = 2;   
                            countSetPoint++;                                             
                        }
                }break;
                
                case 9:{
                        const int tWait = 2000;
                        timeSetPoint += tmSetPoint;                             
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            rollSetPoint = 2;
                            pitchSetPoint = 7;   
                            countSetPoint++;                                             
                        }
                }break;  
                case 10:{
                        const int tWait = 2000;
                        timeSetPoint += tmSetPoint;                            
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            rollSetPoint = -2;
                            pitchSetPoint = 0;   
                            countSetPoint ++;                                             
                        }
                }break;                        

                case 11:{
                        const int tWait = 2000;
                        timeSetPoint += tmSetPoint;                               
                        if (timeSetPoint >= tWait){
                            timeSetPoint = 0;
                            rollSetPoint = 0;
                            pitchSetPoint = 0;   
                            countSetPoint++;                                             
                        }
                }break;                                 




            }  
        }
  
    vTaskDelayUntil(&xLastWakeTime, tm); 
    }
}


/**********************************************************************************************************************************/
/************************************************************** SETUP-LOOP ********************************************************/
/**********************************************************************************************************************************/

// ° CORE 1: SETUP
void setup() { // CORE 1
Serial.begin(115200);
    gpio_config_t io_conf;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = (1 << 2);
    gpio_config(&io_conf);
initCalIMU();
initMotors();
xTaskCreatePinnedToCore(control,"Control", 5 * 1024, (void *) "vTaskDelayUntil_1", 10, NULL, 0);
timer_tg1_initialise(500);
}
// ° CORE 1: SETUP
void loop() { // CORE 1
      switch(countSensor){
            case 0:{ // Init
                    tTimerInicial = tTimerActual; 
            }break;
            case 1:{
                    if ( flagTimer == 1)
                    {   flagTimer = 0;                       
                        tTimer = tTimerActual - tTimerInicial;
                        tTimerInicial = tTimerActual; 
                        leerIMU();
                    }
            }break;

      }  

}