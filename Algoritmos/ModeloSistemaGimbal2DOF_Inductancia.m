clear all
clc

syms Ia1 Ib1  Ic1 % Razones de cambio de las corrientes motor 1 [A/s]
syms Ia2 Ib2  Ic2 % Razones de cambio de las corrientes motor 2 [A/s]
syms R1  R2               % Resistencia de linea [Ohms]
syms Kt1 Kt2              % Constante de torque [N*m/A]
syms b1 b2               % Constante de amortiguamiento (Fricción) [N*m/(Rad/s)]
syms P1  P2               % Número de polos del motor [Dec]
syms u1 u2 % Indices (Ángulos eléctricos de control) [Dec]
syms x1 y1 z1 % Centros de masa eslabon 1 [m]
syms x2 y2 z2 % Centros de masa eslabon 2 [m]
syms q1 q1p q1pp q2 q2p q2pp  % Posición, velocidad, aceleración eslabón 1 y 2 [m], [m/s], [m/s^2]
syms m1 m2 I1 I2 g % Masas, inercias eslabón 1 y 2; gravedad. [kg], [kg*m^2]
syms l1 l2 l3  % Distancias entre origenes de los Sistema de referencia [m]
syms Kv1 Kv2  % ganancia para escalado de amplitud de duty cycle [Dec]
syms Vmax  % Voltaje máximo aplicado a los motores [V]
syms TL1 TL2  % Torque de perturbación en los eslabones 1 y 2 [N*m]
syms H1 H2 % Inductancia en los motores  [H]
syms Kb1 Kb2 % Constante de fuerza contra electromotriz (BEMF) [V/(rad/s)]

% Relación angulo eléctrico y mecánico motor 1 (modifica el BackEMF y la forma de las corrientes de control producidas)
qe1 = P1*q1/2 
% Relación angulo eléctrico y mecánico motor 2 (modifica el BackEMF y la forma de las corrientes de control producidas)
qe2 = P2*q2/2

% Voltajes BEMF motor 1
ea1 = Kb1*q1p*sin(qe1)/2
eb1 = Kb1*q1p*sin(qe1 + 2*pi/3)/2
ec1 = Kb1*q1p*sin(qe1 + 4*pi/3)/2

% Voltajes BEMF motor 2
ea2 = Kb2*q2p*sin(qe2)/2
eb2 = Kb2*q2p*sin(qe2 + 2*pi/3)/2
ec2 = Kb2*q2p*sin(qe2 + 4*pi/3)/2

% Conversión de indice a ángulo eléctrico de las corrientes
qec1 = u1*2*pi/255;
qec2 = u2*2*pi/255;

% Voltajes Aplicados en función de la acción de control u1, Motor 1 [V]
Va1 = 0.5*Kv1*Vmax*sin(qec1)          + 0.5*Vmax
Vb1 = 0.5*Kv1*Vmax*sin(qec1 + 2*pi/3) + 0.5*Vmax
Vc1 = 0.5*Kv1*Vmax*sin(qec1 + 4*pi/3) + 0.5*Vmax

% Voltajes Aplicados en función de la acción de control u1, Motor 2 [V]
Va2 = 0.5*Kv2*Vmax*sin(qec2)          + 0.5*Vmax
Vb2 = 0.5*Kv2*Vmax*sin(qec2 + 2*pi/3) + 0.5*Vmax
Vc2 = 0.5*Kv2*Vmax*sin(qec2 + 4*pi/3) + 0.5*Vmax

Ic1 = -Ia1 - Ib1
Ic2 = -Ia2 - Ib2

% Corrientes de control motor 1 [A]
ia1 = (Va1 - H1*Ia1 - ea1)/R1
ib1 = (Vb1 - H1*Ib1 - eb1)/R1
ic1 = (Vc1 - H1*Ic1 - ec1)/R1

% Corrientes de control motor 2 [A]
ia2 = (Va2 - H2*Ia2 - ea2)/R2
ib2 = (Vb2 - H2*Ib2 - eb2)/R2
ic2 = (Vc2 - H2*Ic2 - ec2)/R2

%Torque a mover en el eslabón 1 (Tomada del modelo de la estructura mecánica)
T1 = I1*q1pp + I2*q1pp + I2*q2pp + b1*q1p + l1^2*m1*q1pp + l1^2*m2*q1pp + l3^2*m2*q1pp + m1*q1pp*x1^2 + m1*q1pp*y1^2 + m2*q1pp*y2^2 + m2*q1pp*z2^2 + g*m2*(l1*sin(q1) - z2*sin(q1) + x2*cos(q1)*cos(q2) + l3*cos(q1)*sin(q2) - y2*cos(q1)*sin(q2)) - g*m1*(y1*cos(q1) - l1*sin(q1) + x1*sin(q1)) - 2*l1*m1*q1pp*x1 - 2*l3*m2*q1pp*y2 - 2*l1*m2*q1pp*z2 - l3^2*m2*q1pp*cos(q2)^2 + m2*q1pp*x2^2*cos(q2)^2 - m2*q1pp*y2^2*cos(q2)^2 + m2*q2pp*y2*z2*cos(q2) - l1*m2*q2pp*x2*sin(q2) + m2*q2pp*x2*z2*sin(q2) + 2*l3*m2*q1pp*y2*cos(q2)^2 + l3*m2*q1pp*x2*sin(2*q2) - m2*q1pp*x2*y2*sin(2*q2) + l1*l3*m2*q2pp*cos(q2) - l1*m2*q2pp*y2*cos(q2) - l3*m2*q2pp*z2*cos(q2);
%Toque a mover en el eslabón 2 (Tomada del modelo de la estructura mecánica)
T2 = I2*q1pp + I2*q2pp + b2*q2p + l3^2*m2*q2pp + m2*q2pp*x2^2 + m2*q2pp*y2^2 - (m2*q1p*(l3^2*q1p*sin(2*q2) - q1p*x2^2*sin(2*q2) + q1p*y2^2*sin(2*q2) - 2*l1*q2p*x2*cos(q2) - 2*l1*l3*q2p*sin(q2) + 2*q2p*x2*z2*cos(q2) + 2*l1*q2p*y2*sin(q2) + 2*l3*q2p*z2*sin(q2) - 2*q2p*y2*z2*sin(q2) + 2*l3*q1p*x2*cos(2*q2) - 2*q1p*x2*y2*cos(2*q2) - 2*l3*q1p*y2*sin(2*q2)))/2 - 2*l3*m2*q2pp*y2 - g*m2*sin(q1)*(y2*cos(q2) - l3*cos(q2) + x2*sin(q2)) + m2*q1pp*y2*z2*cos(q2) - l1*m2*q1pp*x2*sin(q2) + m2*q1pp*x2*z2*sin(q2) + l1*l3*m2*q1pp*cos(q2) - l1*m2*q1pp*y2*cos(q2) - l3*m2*q1pp*z2*cos(q2);

% Dinámica mecánica (Balance de Torques en el eje del motor 1)
ecuMec1 = T1 + TL1 ==  Kt1*( sin(qe1)*ia1 + sin(qe1 + 2*pi/3)*ib1 + sin(qe1 + 4*pi/3)*ic1 ) 
% Dinámica mecánica (Balance de Torques en el eje del motor 2)
ecuMec2 = T2 + TL2 ==  Kt2*( sin(qe2)*ia2 + sin(qe2 + 2*pi/3)*ib2 + sin(qe2 + 4*pi/3)*ic2 ) 

% Se guardan las ecuaciones de corriente
ecuElec_a1 = ia1
ecuElec_b1 = ib1
ecuElec_a2 = ia2
ecuElec_b2 = ib2

% Se realiza la igualdad de las ecuaciones de corriente para despeje de sus
% razones de cambio (Razones de cambio de los estados de corriente)
syms ia1 ib1 ia2 ib2
ecuElec_a1 = ia1 == ecuElec_a1
ecuElec_b1 = ib1 == ecuElec_b1
ecuElec_a2 = ia2 == ecuElec_a2
ecuElec_b2 = ib2 == ecuElec_b2

% Se realiza despeje de las razones de cambio de los estados de los ángulos
[q1pp, q2pp, Ia1, Ib1, Ia2, Ib2] = solve(ecuMec1, ecuMec2, ecuElec_a1, ecuElec_b1, ecuElec_a2, ecuElec_b2, q1pp, q2pp, Ia1, Ib1, Ia2, Ib2);
q1pp = simplify(q1pp) % Razón de cambio q1p
q2pp = simplify(q2pp) % Razón de cambio q2p
Ia1 = simplify(Ia1) % Razón de cambio ia1
Ib1 = simplify(Ib1) % Razón de cambio ib1
Ia2 = simplify(Ia2) % Razón de cambio ia2
Ib2 = simplify(Ib2) % Razón de cambio ib2

x = [q1 ; q1p ; q2 ; q2p ; ia1; ib1; ia2; ib2]       % ESTADOS
X = [q1p; q1pp; q2p; q2pp; Ia1; Ib1; Ia2; Ib2]       % DINÁMICAS (Razones de cambio de los estados)
U = [u1; u2; TL1; TL2]                               % ENTRADAS DE CONTROL 
y = [q1*180/pi; q2*180/pi]                           % SALIDAS

A = jacobian(X,x)                                    % (dX/dx)
B = jacobian(X,U)                                    % (dX/du)
C = jacobian(y,x)                                  % (dy/dx) [Rad]
D = jacobian(y,U)                                    % (dy/du)

%% Substitucion de variables por u()
% Antes de correr, crear una carpeta llamada data en donde se encuentra 
% este script.
% Luego de correr este scrip podrá ver en la carpeta data las ecuaciones de
% las razones de cambio de los estado. Posteriormente corra el archivo
% changeToU.py y, en la misma carpeta, podrá ver las ecuaciones de las
% razones de cambio de los estados, terminado _u, con el cambio de variable 
% respectivo.
variables = ["q1"  , "q1p" , "q2"  , "q2p" , "ia1" , "ib1" , "ia2" , "ib2" , "u1"  , "u2"   , "TL1"  , "TL2"];
reemplazo = ["u(1)", "u(2)", "u(3)", "u(4)", "u(5)", "u(6)", "u(7)", "u(8)", "u(9)", "u(10)", "u(11)", "u(12)"];
ecuaciones = ["X1", "X2", "X3", "X4", "X5", "X6", "X7", "X8"];

eqs = length(ecuaciones)
vars = length(variables)

fileMeta = './data/metadata.txt'
if exist(fileMeta, 'file')== 2
  delete(fileMeta);
end
varMeta = "8"
diary(fileMeta)
   varMeta
diary off

fprintf('%s\n','Ecuaciones X:')
for i = 1:eqs 
    eq_str = string(X(i));
    file = './data/' + ecuaciones(i) + '.txt';
    if exist(file, 'file')==2
      delete(file);
    end
    diary(file)
       eq_str
    diary off
end