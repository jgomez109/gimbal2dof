clear all
close all
clc

PID = load('PID.txt');
LQG = load('LQG.txt');
MIX = load('MIX.txt');

tpid = PID(:,1)./1000;
tlqg = LQG(:,1)./1000;
tmix = MIX(:,1)./1000;

sp2pid = PID(:,3);
sp2lqg = LQG(:,3);
sp2mix = MIX(:,3);

sp1pid =  PID(:,5);
sp1lqg = LQG(:,5);
sp1mix = MIX(:,5);

q1pid = PID(:,6);
q1lqg = LQG(:,6);
q1mix = MIX(:,6);

q2pid = PID(:,4);
q2lqg = LQG(:,4);
q2mix = MIX(:,4);

u1pid = PID(:,7);
u1lqg = LQG(:,7);
u1mix = MIX(:,7);

u2pid = PID(:,8);
u2lqg = LQG(:,8);
u2mix = MIX(:,8);

% figure(1)
% plot(tpid, sp2pid, tlqg, sp2lqg, tmix, sp2mix)
% legend('pid', 'lqg', 'mix')
% figure(11)
% plot(tpid, sp1pid, tlqg, sp1lqg, tmix, sp1mix)
% legend('pid', 'lqg', 'mix')

%% Ajuste de tiempo
tlqg_ = tlqg + 0.03;

% figure(2)
% plot(tlqg, sp2lqg, tlqg_, sp2lqg)
% legend('lqg', 'lqg_')

tmix_ = tmix + 0.08;

% figure(3)
% plot(tmix, sp2mix, tmix_, sp2mix)
% legend('mix', 'mix_')
% 
% figure(4)
% plot(tpid, sp2pid, tlqg_, sp2lqg, tmix_, sp2mix)
% legend('pid', 'lqg_', 'mix_')

%% Comparación
figure(10)
subplot(2,1,1)
plot(tpid, sp1pid,'--k', tpid, q1pid, 'b', tlqg_, q1lqg, tmix_, q1mix, 'g');
grid on
title('Eslabón 1')
ylabel('Posición [Grados]')
% xlim([5, 15])
xlim([8, 8.7])
ylim([-0.5, 31.5])
legend('Set Point', 'PID', 'LQG', 'MixSyn')
subplot(2,1,2)
plot(tpid, u1pid, tlqg_, u1lqg, tmix_, u1mix)
grid on
ylabel('Control [Dec]')
xlabel('Tiempo [s]')
% xlim([5, 15])
xlim([8, 8.7])
% ylim([-1000 1000])
ylim([-200 200])
legend('PID', 'LQG', 'MixSyn')

figure(11)
subplot(2,1,1)
plot(tpid, sp2pid,'--k', tpid, q2pid, 'b', tlqg_, q2lqg, tmix_, q2mix, 'g');
grid on
title('Eslabón 2')
ylabel('Posición [Grados]')
xlim([5, 15])
legend('Set Point', 'PID', 'LQG', 'MixSyn')
subplot(2,1,2)
plot(tpid, u2pid, tlqg_, u2lqg, tmix_, u2mix)
grid on
ylabel('Control [Dec]')
xlabel('Tiempo [s]')
xlim([5, 15])
ylim([-1000 1000])
legend('PID', 'LQG', 'MixSyn')