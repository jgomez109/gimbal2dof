clear all
clc

syms Va1 Vb1 Vc1 Va2 Vb2 Vc2   % Voltajes de linea
syms ia1 ib1 ic1 ia2 ib2 ic2        % Corrientes de linea
syms te1 te2           % Ángulo eléctrico(te)
syms Te1 Tl1 Te2 Tl2            % Torque eléctrico y carga
syms R1  R2               % Iesistencia de linea
syms Kt1 Kt2              % Constante de torque [N*m/A]
syms b1 b2               % Constante de amortiguamiento (Fricción) [N*m/(Rad/s)]
syms P1  P2               % Número de polos del motor

syms u1 u2 %Indices (Ángulos eléctricos de control)

syms x1 y1 z1 % Centros de masa eslabon 1
syms x2 y2 z2 % Centros de masa eslabon 2
syms q1 q1p q1pp q2 q2p q2pp  % Posición, velocidad, aceleración eslabón 1 y 2
syms m1 m2 I1 I2 g % Masas, inercias eslabón 1 y 2; gravedad.
syms l1 l2 l3  % Distancias entre origenes de los Sistema de referencia
syms Kv1 Kv2
syms Vmax

% Relación angulo eléctrico y mecánico motor 1
qe1 = P1*q1/2 
% Relación angulo eléctrico y mecánico motor 2
qe2 = P2*q2/2

% Conversión de indice ángulo
ae1 = u1*2*pi/256;
ae2 = u2*2*pi/256;

% Voltajes Aplicados en función de la acción de control u1, Motor 1
Va1 = 0.5*Kv1*Vmax*sin(ae1)          + 0.5*Vmax
Vb1 = 0.5*Kv1*Vmax*sin(ae1 + 2*pi/3) + 0.5*Vmax
Vc1 = 0.5*Kv1*Vmax*sin(ae1 + 4*pi/3) + 0.5*Vmax

% Voltajes Aplicados en función de la acción de control u1, Motor 2
Va2 = 0.5*Kv2*Vmax*sin(ae2)          + 0.5*Vmax
Vb2 = 0.5*Kv2*Vmax*sin(ae2 + 2*pi/3) + 0.5*Vmax
Vc2 = 0.5*Kv2*Vmax*sin(ae2 + 4*pi/3) + 0.5*Vmax

ia1 = Va1/R1
ib1 = Vb1/R1
ic1 = Vc1/R1

ia2 = Va2/R2
ib2 = Vb2/R2
ic2 = Vc2/R2

%Torque a mover en el eslabón 1
T1 = I1*q1pp + I2*q1pp + I2*q2pp + b1*q1p + l1^2*m1*q1pp + l1^2*m2*q1pp + l3^2*m2*q1pp + m1*q1pp*x1^2 + m1*q1pp*y1^2 + m2*q1pp*y2^2 + m2*q1pp*z2^2 + g*m2*(l1*sin(q1) - z2*sin(q1) + x2*cos(q1)*cos(q2) + l3*cos(q1)*sin(q2) - y2*cos(q1)*sin(q2)) - g*m1*(y1*cos(q1) - l1*sin(q1) + x1*sin(q1)) - 2*l1*m1*q1pp*x1 - 2*l3*m2*q1pp*y2 - 2*l1*m2*q1pp*z2 - l3^2*m2*q1pp*cos(q2)^2 + m2*q1pp*x2^2*cos(q2)^2 - m2*q1pp*y2^2*cos(q2)^2 + m2*q2pp*y2*z2*cos(q2) - l1*m2*q2pp*x2*sin(q2) + m2*q2pp*x2*z2*sin(q2) + 2*l3*m2*q1pp*y2*cos(q2)^2 + l3*m2*q1pp*x2*sin(2*q2) - m2*q1pp*x2*y2*sin(2*q2) + l1*l3*m2*q2pp*cos(q2) - l1*m2*q2pp*y2*cos(q2) - l3*m2*q2pp*z2*cos(q2);
%Toque a mover en el eslabón 2
T2 = I2*q1pp + I2*q2pp + b2*q2p + l3^2*m2*q2pp + m2*q2pp*x2^2 + m2*q2pp*y2^2 - (m2*q1p*(l3^2*q1p*sin(2*q2) - q1p*x2^2*sin(2*q2) + q1p*y2^2*sin(2*q2) - 2*l1*q2p*x2*cos(q2) - 2*l1*l3*q2p*sin(q2) + 2*q2p*x2*z2*cos(q2) + 2*l1*q2p*y2*sin(q2) + 2*l3*q2p*z2*sin(q2) - 2*q2p*y2*z2*sin(q2) + 2*l3*q1p*x2*cos(2*q2) - 2*q1p*x2*y2*cos(2*q2) - 2*l3*q1p*y2*sin(2*q2)))/2 - 2*l3*m2*q2pp*y2 - g*m2*sin(q1)*(y2*cos(q2) - l3*cos(q2) + x2*sin(q2)) + m2*q1pp*y2*z2*cos(q2) - l1*m2*q1pp*x2*sin(q2) + m2*q1pp*x2*z2*sin(q2) + l1*l3*m2*q1pp*cos(q2) - l1*m2*q1pp*y2*cos(q2) - l3*m2*q1pp*z2*cos(q2);

% Dinámica mecánica (Balance de Torques en el eje del motor 1)
ecu5 = T1 ==  Kt1*( sin(qe1)*ia1 + sin(qe1 + 2*pi/3)*ib1 + sin(qe1 + 4*pi/3)*ic1 )
% Dinámica mecánica (Balance de Torques en el eje del motor 2)
ecu6 = T2 ==  Kt2*( sin(qe2)*ia2 + sin(qe2 + 2*pi/3)*ib2 + sin(qe2 + 4*pi/3)*ic2 )

[q1pp,q2pp] = solve(ecu5,ecu6,q1pp,q2pp);
q1pp = simplify(q1pp) % Razón de cambio q1p
q2pp = simplify(q2pp) % Razón de cambio q2p

x = [q1 ; q1p ; q2 ; q2p]       % ESTADOS
X = [q1p; q1pp; q2p; q2pp]       % DINÁMICAS
u = [u1; u2]       % ENTRADAS DE CONTROL 
y = [q1; q2] % SALIDAS

A = jacobian(X,x)       % (dX/dx)
B = jacobian(X,u)       % (dX/du)
% C = jacobian(y,x)       % (dy/dx) 
C = [1*180/pi 0 0 0
     0        0 180/pi 0]
D = jacobian(y,u)       % (dy/du)